module SParserSpec where

import           Test.Hspec

import           AParser    (satisfy)
import           Data.Char  (isUpper)
import           SParser

spec :: Spec
spec = describe "SParser" $ do
         it "have (+) and (*)" $ do
           runParser (zeroOrMore (satisfy isUpper)) "ABCdEfgH" `shouldBe` Just ("ABC","dEfgH")
           runParser (oneOrMore (satisfy isUpper)) "ABCdEfgH" `shouldBe` Just ("ABC","dEfgH")
           runParser (zeroOrMore (satisfy isUpper)) "abcdeFGh" `shouldBe` Just ("","abcdeFGh")
           runParser (oneOrMore (satisfy isUpper)) "abcdeFGh" `shouldBe` Nothing
         it "can parse identifiers" $ do
           runParser ident "foobar baz" `shouldBe` Just ("foobar"," baz")
           runParser ident "foo33fA" `shouldBe` Just ("foo33fA","")
           runParser ident "2bad" `shouldBe` Nothing
           runParser ident "" `shouldBe` Nothing
         it "can parse S expressions" $ do
           runParser parserSExpr "5" `shouldBe` Just (A (N 5), "")
           runParser parserSExpr "foo3" `shouldBe` Just (A (I "foo3"), "")
           runParser parserSExpr "(bar (foo) 3 5 874)"
             `shouldBe` Just (Comb [A (I "bar"), Comb [A (I "foo")], A (N 3), A (N 5), A (N 874)], "")
           runParser parserSExpr "( lots of ( spaces in ) this ( one ) )"
             `shouldBe` Just (Comb [A (I "lots")
                                  , A (I "of")
                                  , Comb [A (I "spaces"), A (I "in")]
                                  , A (I "this")
                                  , Comb [A (I "one")]], "")
