module NondeterministicCalculationsSpec where

import           Data.List                    (nub,sort)
import qualified Data.List                    (permutations)

import           NondeterministicCalculations
import           Test.Hspec
import           Test.QuickCheck

spec :: Spec
spec = describe "NondeterministicCalculations" $ do
         describe "bin" $ do
           it "works on samples" $ do
             sort (bin 1) `shouldBe` sort [ [ 0 ] , [ 1 ] ]
             sort (bin 2) `shouldBe` sort [ [ 0 , 0 ] , [ 1 , 0 ] , [ 0 , 1 ] , [ 1 , 1 ] ]
             sort (bin 3) `shouldBe` sort [ [ 0 , 0 , 0 ]
                                          , [ 1 , 0 , 0 ]
                                          , [ 0 , 1 , 0 ]
                                          , [ 1 , 1 , 0 ]
                                          , [ 0 , 0 , 1 ]
                                          , [ 1 , 0 , 1 ]
                                          , [ 0 , 1 , 1 ]
                                          , [ 1 , 1 , 1 ]
                                          ]
           it "is safe" $ do
             bin 0 `shouldBe` [[]]
             bin (-1 :: Int) `shouldBe` []
           it "generates all sequences" $ do
             length (bin 4) `shouldBe` (2^4)
             length (nub $ bin 4) `shouldBe` (2^4)
           it "generates sequences of proper length" $
             all (== 5) (map length (bin 5)) `shouldBe` True
         describe "combinations" $ do
           it "works on sample" $
             sort (map sort $ combinations 4 2) `shouldBe` sort (map sort [ [1, 2]
                                                                          , [1, 3]
                                                                          , [1, 4]
                                                                          , [2, 3]
                                                                          , [2, 4]
                                                                          , [3, 4]
                                                                          ])
           it "is safe" $ do
             combinations 5 10 `shouldBe` []
             combinations (negate 2) (negate 1) `shouldBe` []
             combinations 2 (negate 1) `shouldBe` []
             combinations (negate 2) 1 `shouldBe` []
             combinations 5 0 `shouldBe` [[]]
           it "has sublists of same size" $
             property $ \a b -> let n = a `mod` 7
                                    k = b `mod` 7
                                in all (if k >= 0 && n >= k
                                        then (== k)
                                        else (== 1)) $ map length $ combinations n k
           it "has length of n!/k!(n-k)!" $
             property $ \a b -> let n = a `mod` 10
                                    k = b `mod` 10
                                in length (combinations n k) == if k >= 0 && n >= k
                                                                then prod (k+1) n `div` prod 1 (n-k)
                                                                else 0
         describe "permutations" $ do
           it "works on sample" $
             sort (permutations [22, 10, 5]) `shouldBe` sort [ [22, 10, 5]
                                                             , [22, 5, 10]
                                                             , [10, 22, 5]
                                                             , [10, 5, 22]
                                                             , [5, 22, 10]
                                                             , [5, 10, 22]
                                                             ]
           it "produces results same as Data.List.permutations" $
             property
               ((\lst -> let l = take 7 lst in
                 sort (Data.List.permutations l)
                   == sort (NondeterministicCalculations.permutations l)) :: [Int] -> Bool)
  where
    prod f t = if f <= t then product [f..t] else 1