module MonstuparSpec where

import Data.Char (isUpper)
import Data.Either (isLeft)

import Test.Hspec

import Monstupar

spec :: Spec
spec = describe "Monstupar" $ do
         describe "samples" $
           it "is rewritten correct" $ do
             runParser (satisfy isUpper) "ABC" `shouldBe` Right ("BC", 'A')
             isLeft (runParser  (satisfy isUpper) "abc") `shouldBe` True
             runParser (exactly 'x') "xyz" `shouldBe` Right ("yz", 'x')
         describe "task 3" $
           it "passes tests from pdf" $ do
             runParser abParser "abcdef" `shouldBe` Right ("cdef",('a','b'))
             isLeft (runParser abParser "aebcdf") `shouldBe` True
             runParser abParser_ "abcdef" `shouldBe` Right ("cdef",())
             isLeft (runParser abParser_ "aebcdf") `shouldBe` True
             runParser intPair "12 34" `shouldBe` Right ("",[12,34])
         describe "task 5" $
           it "passes tests from pdf" $ do
             runParser intOrUppercase "342abcd" `shouldBe` Right ("abcd", ())
             runParser intOrUppercase "XYZ" `shouldBe` Right ("YZ", ())
             isLeft (runParser intOrUppercase "foo") `shouldBe` True
