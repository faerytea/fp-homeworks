module AParserSpec where

import           Data.Char(isUpper)

import           Test.Hspec

import           AParser

spec :: Spec
spec = describe "AParser" $ do
         describe "samples" $
           it "is rewritten correct" $ do
             runParser (satisfy isUpper) "ABC" `shouldBe` Just ('A',"BC")
             runParser (satisfy isUpper) "abc" `shouldBe` Nothing
             runParser (char 'x') "xyz" `shouldBe` Just ('x',"yz")
         describe "task 3" $
           it "passes tests from pdf" $ do
             runParser abParser "abcdef" `shouldBe` Just (('a','b'),"cdef")
             runParser abParser "aebcdf" `shouldBe` Nothing
             runParser abParser_ "abcdef" `shouldBe` Just ((),"cdef")
             runParser abParser_ "aebcdf" `shouldBe` Nothing
             runParser intPair "12 34" `shouldBe` Just ([12,34],"")
         describe "task 5" $
           it "passes tests from pdf" $ do
             runParser intOrUppercase "342abcd" `shouldBe` Just ((), "abcd")
             runParser intOrUppercase "XYZ" `shouldBe` Just ((), "YZ")
             runParser intOrUppercase "foo" `shouldBe` Nothing