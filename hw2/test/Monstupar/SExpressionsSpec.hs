module Monstupar.SExpressionsSpec where

import Data.Char(isUpper)
import Data.Either(isLeft)

import Test.Hspec

import Monstupar
import Monstupar.SExpressions

spec :: Spec
spec = describe "Monstupar SParser" $ do
         it "have (+) and (*)" $ do
           runParser (zeroOrMore (satisfy isUpper)) "ABCdEfgH" `shouldBe` Right ("dEfgH","ABC")
           runParser (oneOrMore (satisfy isUpper)) "ABCdEfgH" `shouldBe` Right ("dEfgH","ABC")
           runParser (zeroOrMore (satisfy isUpper)) "abcdeFGh" `shouldBe` Right ("abcdeFGh","")
           isLeft (runParser (oneOrMore (satisfy isUpper)) "abcdeFGh") `shouldBe` True
         it "can parse identifiers" $ do
           runParser ident "foobar baz" `shouldBe` Right (" baz","foobar")
           runParser ident "foo33fA" `shouldBe` Right ("","foo33fA")
           isLeft (runParser ident "2bad") `shouldBe` True
           isLeft (runParser ident "") `shouldBe` True
         it "can parse S expressions" $ do
           runParser parserSExpr "5" `shouldBe` Right ("", A (N 5))
           runParser parserSExpr "foo3" `shouldBe` Right ("", A (I "foo3"))
           runParser parserSExpr "(bar (foo) 3 5 874)"
             `shouldBe` Right ("", Comb [A (I "bar"), Comb [A (I "foo")], A (N 3), A (N 5), A (N 874)])
           runParser parserSExpr "( lots of ( spaces in ) this ( one ) )"
             `shouldBe` Right ("", Comb [A (I "lots")
                                       , A (I "of")
                                       , Comb [A (I "spaces"), A (I "in")]
                                       , A (I "this")
                                       , Comb [A (I "one")]])
