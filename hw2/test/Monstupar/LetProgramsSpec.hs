module Monstupar.LetProgramsSpec where

import Data.Maybe (fromJust)

import Test.Hspec

import Monstupar
import Monstupar.LetPrograms
import ParserM(optimizeLet)

letProgramExample :: String
letProgramExample = "let x = 1 + 2 + 5  \n"
                 ++ "let   y = x+x       \n\n"
                 ++ "let z=0+    x   + y + 8\n"

letProgramExampleOptimized :: String
letProgramExampleOptimized = "let x = 8\n"
                          ++ "let y = 16\n"
                          ++ "let z = 32"

spec :: Spec
spec = describe "Monstupar let parser" $
         it "can do thing from example" $
           show (fromJust $ optimizeLet $ unwrap $ runParser parserLetProgram letProgramExample)
             `shouldBe` letProgramExampleOptimized
             where
               unwrap (Right (rest,p)) = if null rest then p else undefined