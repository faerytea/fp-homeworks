module OptionalMonadSpec where

import OptionalMonad
import Test.Hspec
import Test.QuickCheck

spec :: Spec
spec = describe "OptionalMonad" $
         describe "Expr" $ do
           it "eval (Const x) == x" $
             property $ \x -> eval (Const x) == Right x
           it "eval ((Const a) `Add` (Const b)) == a + b" $
             property $ \x y -> eval (Const x `Add` Const y) == Right (x + y)
           it "eval ((Const a) `Sub` (Const b)) == a - b" $
             property $ \x y -> eval (Const x `Sub` Const y) == Right (x - y)
           it "eval ((Const a) `Mul` (Const b)) == a * b" $
             property $ \x y -> eval (Const x `Mul` Const y) == Right (x * y)
           it "eval ((Const a) `Div` (Const b)) == a `div` b" $
             property $ \x t -> let y = abs t + 1 in eval (Const x `Div` Const y) == Right (x `div` y)
           it "eval ((Const a) `Mod` (Const b)) == a `mod` b" $
             property $ \x t -> let y = abs t + 1 in eval (Const x `Mod` Const y) == Right (x `mod` y)
           it "eval ((Const a) `Exp` (Const b)) == a ^ b" $
             property $ \x t -> let y = abs t in eval (Const x `Exp` Const y) == Right (x ^ y)
           describe "But also it is safe: " $ do
             it "x `div` 0" $
               let expr = (Const 4 `Div` Const 0)
                 in eval expr `shouldBe` (Left $ ArithmeticError "division by zero" expr)
             it "x `mod` 0" $
               let expr = (Const 4 `Mod` Const 0)
                 in eval expr `shouldBe` (Left $ ArithmeticError "division by zero" expr)
             it "x ^ -1" $
               let expr = (Const 4 `Exp` Const (-1 :: Int))
                 in eval expr `shouldBe` (Left $ ArithmeticError "negative exponent" expr)
           it "also there is nice show: ((((7 + 4) ^ 2) % (0 ^ 0)) + 7)" $
             show ((Const 7 `Add` Const 4 `Exp` Const 2) `Mod` (Const 0 `Exp` Const 0) `Add` Const 7)
               `shouldBe` "((((7 + 4) ^ 2) % (0 ^ 0)) + 7)"
           it "can calculate previous expression" $
             eval ((Const 7 `Add` Const 4 `Exp` Const 2) `Mod` (Const 0 `Exp` Const 0) `Add` Const 7)
               `shouldBe` Right ((((7 + 4) ^ 2) `mod` (0 ^ 0)) + 7)