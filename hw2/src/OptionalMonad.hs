{-# LANGUAGE ExplicitNamespaces #-}
{-# LANGUAGE RecordWildCards    #-}
{-# LANGUAGE TypeOperators      #-}

module OptionalMonad
       ( Expr(Const, Add, Sub, Mul, Div, Mod, Exp)
       , ArithmeticError(..)
       , eval
       , type (~>)(..)
       , partial
       , total
       , apply
       , applyOrElse
       , withDefault
       , isDefinedAt
       , orElse
       ) where

import           Control.Applicative (liftA2)
import qualified Control.Category    as Cat (Category (id, (.)))
import           Data.Maybe          (fromMaybe, isJust)

data Expr = Const Int
          | Add Expr Expr
          | Sub Expr Expr
          | Mul Expr Expr
          | Div Expr Expr
          | Mod Expr Expr
          | Exp Expr Expr
          deriving Eq

instance Show Expr where
  show (Const c) = show c
  show (Add l r) = '(':show l ++ " + " ++ show r ++ ")"
  show (Sub l r) = '(':show l ++ " - " ++ show r ++ ")"
  show (Mul l r) = '(':show l ++ " * " ++ show r ++ ")"
  show (Div l r) = '(':show l ++ " / " ++ show r ++ ")"
  show (Mod l r) = '(':show l ++ " % " ++ show r ++ ")"
  show (Exp l r) = '(':show l ++ " ^ " ++ show r ++ ")"

data ArithmeticError = ArithmeticError
                     { aeMessage :: String
                     , aeNode    :: Expr
                     } deriving Eq

instance Show ArithmeticError where
  show ArithmeticError {..} = "Error: " ++ aeMessage ++ " in expression " ++ show aeNode

eval :: Expr -> Either ArithmeticError Int
eval (Const a) = Right a
eval (Add a b) = liftA2 (+) (eval a) (eval b)
eval (Sub a b) = liftA2 (-) (eval a) (eval b)
eval (Mul a b) = liftA2 (*) (eval a) (eval b)
eval (Div a b) = if eval b == Right 0
                 then Left $ ArithmeticError "division by zero" (Div a b)
                 else liftA2 div (eval a) (eval b)
eval (Mod a b) = if eval b == Right 0
                 then Left $ ArithmeticError "division by zero" (Mod a b)
                 else liftA2 mod (eval a) (eval b)
eval (Exp a b) = if ((< 0) <$> eval b) == Right True
                 then Left $ ArithmeticError "negative exponent" (Exp a b)
                 else liftA2 (^) (eval a) (eval b)

data a ~> b = Partial   (a -> Maybe b) -- a partial function
            | Defaulted (a ~> b) b     -- a partial function with a default value

instance Cat.Category (~>) where
  id = total Prelude.id
  (.) f g = Partial $ \x -> apply g x >>= apply f

partial :: (a -> Maybe b) -> a ~> b
partial = Partial

total :: (a -> b) -> a ~> b
total a = Partial $ \x -> Just $ a x

apply :: (a ~> b) -> a -> Maybe b
apply (Partial f) a     = f a
apply (Defaulted f d) a = maybe (Just d) Just $ apply f a

applyOrElse :: (a ~> b) -> a -> b -> b
applyOrElse f a d = fromMaybe d $ apply f a

-- | Add a default value to a partial function. If the function was already
-- defaulted, override the value with the new default.
withDefault :: (a ~> b) -> b -> (a ~> b)
withDefault pf@(Partial _)  = Defaulted pf
withDefault (Defaulted f _) = Defaulted f

isDefinedAt :: (a ~> b) -> a -> Bool
isDefinedAt f x = isJust $ apply f x

-- | Create a new partial function where the domain is the combination
-- of both partial functions. Priority is given to the first partial function
-- in case of conflict.
orElse :: (a ~> b) -> (a ~> b) -> a ~> b
orElse pf1 pf2 = Partial $ \x -> maybe (apply pf2 x) Just $ apply pf1 x

{- RULES PROOF
   apply (id . p) v
   ≡ apply ((total Prelude.id) . p) v
   ≡ apply (Partial (\x -> apply p x >>= apply (total Prelude.id))) v
   ≡ apply (Partial (\x -> apply p x >>= apply (Partial (\y -> Just (Prelude.id y))))) v
   ≡ apply (Partial (\x -> apply p x >>= apply (Partial (\y -> Just y)))) v
   ≡ apply (Partial (\x -> apply p x >>= apply (Partial Just))) v
   ≡ apply (Partial (\x -> apply p x >>= Just)) v
   ≡ apply (Partial (\x -> case apply p x of
                        Just r -> Just r
                        Nothing -> Nothing)) v
   ≡ apply (Partial (\x -> apply p x)) v
   ≡ (\x -> apply p x) v
   ≡ apply p v

   apply (p . id) v
   ≡ apply (p . (total Prelude.id)) v
   ≡ apply (Partial $ \x -> apply (total Prelude.id) x >>= apply p) v
   ≡ apply (Partial $ \x -> apply (Partial (\y -> Just (Prelude.id y))) x >>= apply p) v
   ≡ apply (Partial $ \x -> apply (Partial (\y -> Just y)) x >>= apply p) v
   ≡ apply (Partial $ \x -> apply (Partial Just) x >>= apply p) v
   ≡ apply (Partial $ \x -> Just x >>= apply p) v
   ≡ apply (Partial $ \x -> apply p x) v
   ≡ (\x -> apply p x) v
   ≡ apply p v

   apply ((p . q) . r) v
   ≡ apply ((Partial $ \x -> apply q x >>= apply p) . r) v
   ≡ apply (Partial $ \x -> apply r x >>= apply (Partial $ \y -> apply q y >>= apply p)) v
   ≡ apply (Partial $ \x -> apply r x >>= (\y -> apply q y >>= apply p)) v
   ≡ apply (Partial $ \x -> (apply r x >>= apply q) >>= apply p) v
   ≡ apply (Partial $ \x -> ((\y -> apply r y >>= apply q) x) >>= apply p) v
   ≡ apply (Partial $ \x -> (apply (Partial $ \y -> apply r y >>= apply q) x) >>= apply p) v
   ≡ apply (Partial $ \x -> (apply (r . q) x) >>= apply p) v
   ≡ apply ((r . q) . p) v
-}
