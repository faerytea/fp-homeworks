module Monstupar.SExpressions
       ( ident
       , Ident
       , Atom(N, I)
       , SExpr(A, Comb)
       , parserSExpr
       ) where

import           Data.Char           (isAlpha, isAlphaNum)

import           Control.Applicative ((<|>))
import           Monstupar           (Monstupar, exactly, oneOrMore, posInt, satisfy,
                                      spaces, zeroOrMore)
import           SParser             (Atom (I, N), Ident, SExpr (A, Comb))

ident :: Monstupar Char String
ident = ((:) <$> satisfy isAlpha) <*> zeroOrMore (satisfy isAlphaNum)

atom :: Monstupar Char Atom
atom = (I <$> ident) <|> (N <$> posInt)

parserSExpr :: Monstupar Char SExpr
parserSExpr = spaces
            *> (Comb <$> (exactly '(' *> ((:) <$> parserSExpr)
            <*> zeroOrMore (oneOrMore (exactly ' ') *> parserSExpr) <* exactly ')')
            <|> A <$> atom)
            <* spaces
