module Monstupar.LetPrograms
       ( parserLetProgram
       , LetProgram(LetProgram)
       , LetExpr(LetExpr)
       , Expr(Expr)
       , Atom(N, I)
       , Ident
       ) where

import           Control.Applicative    (Alternative (..))

import           Monstupar              (Monstupar, exactly, posInt, spaces, zeroOrMore,
                                         (?|))
import           Monstupar.SExpressions (Ident, ident)
import           ParserM                (Atom (I, N), Expr (Expr), LetExpr (LetExpr),
                                         LetProgram (LetProgram))

sign :: Monstupar Char (Integer -> Integer)
sign = (\x -> if x == '+' then (1 *) else ((-1 :: Integer) *)) <$> (exactly '+' <|> exactly '-')

int :: Monstupar Char Integer
int = (sign ?| (1*)) <*> posInt

except :: Monstupar s a -> (a -> Bool) -> Monstupar s a
except parser predicate = parser >>= (\r -> if predicate r then empty else pure r)

atom :: Monstupar Char Atom
atom = (I <$> (ident `except` (== "let"))) <|> (N <$> int)

expr :: Monstupar Char Expr
expr = Expr <$>
       (spaces *> ((:) <$> atom) <*> zeroOrMore (spaces *> exactly '+' *> spaces *> atom) <* spaces)

word :: String -> Monstupar Char String
word = foldr (\c p -> ((:) <$> exactly c) <*> p) (pure "")

parserLetExpr :: Monstupar Char LetExpr
parserLetExpr = spaces *> word "let" *> spaces *>
                (LetExpr <$> ident) <* spaces <* exactly '=' <* spaces <*> expr

parserLetProgram :: Monstupar Char LetProgram
parserLetProgram = LetProgram <$> zeroOrMore parserLetExpr
