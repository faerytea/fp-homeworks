module NondeterministicCalculations
       ( bin
       , combinations
       , permutations
       ) where

bin :: Integral a => a -> [[Int]]
bin n
  | n > 0 = bin (n-1) >>= (\l -> [0:l, 1:l])
  | n == 0 = [[]]
  | otherwise = []

combinations :: Integral a => a -> a -> [[a]]
combinations n k
  | k > 1 = combinations n (k - 1) >>= (\(e:ls) -> map (:e:ls) [(e+1)..n])
  | k == 1 = map (:[]) [1..n]
  | k == 0 = [[]]
  | otherwise = []

-- TODO performance
permutations :: [a] -> [[a]]
permutations lst = perm $ genFirst lst
  where
    perm :: [(a, [a])] -> [[a]]
    perm [] = [[]]
    perm ls = ls >>= (\(e, l) -> map (e :) (permutations l))
    genFirst :: [a] -> [(a, [a])]
    genFirst = helper []
      where
        helper _ []    = []
        helper d (e:r) = (e, d ++ r) : helper (e : d) r
