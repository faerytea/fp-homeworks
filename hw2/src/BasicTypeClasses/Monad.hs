{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE NoImplicitPrelude    #-}
{-# LANGUAGE UndecidableInstances #-}

module BasicTypeClasses.Monad where

import           BasicTypeClasses.Classes (Functor (fmap), Monad (return, (>>=)),
                                           MonadFish (returnFish, (>=>)),
                                           MonadJoin (join, returnJoin), id, (.))

instance Monad m => Functor m where
    fmap f a = a >>= (return . f)

    {- PROOF fmap id ≡ id
       fmap id
       ≡ \x -> fmap id x
       ≡ \x -> x >>= (return . id)
       ≡ \x -> x >>= return
       ≡ \x -> x
       ≡ id
    -}

    {- PROOF fmap f . fmap g ≡ fmap (f . g)
       fmap f . fmap g
       ≡ \x -> (fmap f . fmap g) x
       ≡ \x -> fmap f (fmap g x)
       ≡ \x -> fmap f (x >>= (return . g))
       ≡ \x -> (x >>= (return . g)) >>= (return . f)
       ≡ \x -> x >>= (\y -> (return . g) y >>= (return . f))
       ≡ \x -> x >>= (\y -> return (g y) >>= (return . f))
       ≡ \x -> x >>= (\y -> (return . f) (g y))
       ≡ \x -> x >>= (\y -> return (f (g y)))
       ≡ \x -> x >>= (\y -> return ((f . g) y))
       ≡ \x -> x >>= (\y -> (return . (f . g)) y))
       ≡ \x -> x >>= (return . (f . g)))
       ≡ \x -> fmap (f . g) x
       ≡ fmap (f . g)
    -}

instance Monad m => MonadFish m where
    returnFish = return
    f >=> g  = \x -> f x >>= g

    {- PROOF f >=> returnFish ≡ f
       m >>= return ≡ m                   (law 1)
       f a >>= return ≡ f a               (f a ≡ m | f :: a -> m a, a :: a)
       f a >>= returnFish ≡ f a           (def)
       (\x -> f x >>= returnFish) a ≡ f a (obvious)
       (f >=> returnFish) a ≡ f a         (def)
       f >=> returnFish ≡ f               (eta-reduction)
    -}

instance Monad     m => MonadJoin m where
    returnJoin = return
    join x = x >>= id

    {- PROOF join . fmap join ≡ join . join
       join . fmap join
       ≡ \m -> (join . fmap join) m
       ≡ \m -> join $ fmap join m
       ≡ \m -> join $ m >>= (return . join)
       ≡ \m -> m >>= (return . join) >>= id
       ≡ \m -> m >>= (\x -> (return . join) x >>= id)
       ≡ \m -> m >>= (\x -> return (join x) >>= id)
       ≡ \m -> m >>= (\x -> id (join x))
       ≡ \m -> m >>= (\x -> join x)
       ≡ \m -> m >>= (\x -> x >>= id)
       ≡ \m -> m >>= (\x -> id x >>= id)
       ≡ \m -> m >>= id >>= id
       ≡ \m -> join m >>= id
       ≡ \m -> join (join m)
       ≡ \m -> (join . join) m
       ≡ join . join
    -}
