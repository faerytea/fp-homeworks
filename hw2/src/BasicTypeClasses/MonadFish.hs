{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE NoImplicitPrelude    #-}
{-# LANGUAGE UndecidableInstances #-}

module BasicTypeClasses.MonadFish where

import           BasicTypeClasses.Classes (Monad (return, (>>=)),
                                           MonadFish (returnFish, (>=>)),
                                           MonadJoin (join, returnJoin), id)

instance MonadFish m => Monad m where
    return = returnFish
    (>>=) a f = (id >=> f) a

    {- PROOF m >>= return ≡ m
       m >>= return
       ≡ m >>= returnFish
       ≡ (id >=> returnFish) m
       ≡ id m
       ≡ m
    -}

instance MonadFish m => MonadJoin m where
    returnJoin = returnFish
    join = id >=> id

    {- PROOF
       (cannot prove directly, but MonadFish m => Monad m => MonadJoin m)
    -}
