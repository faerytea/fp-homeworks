module BasicTypeClasses.ChaoticGood where

import           Prelude (Applicative (..), Foldable (..), Functor (..),
                          Monoid (mappend, mempty), Traversable (..), ($), (.), (<$>))

 -- Identity

newtype Identity a = Identity { runIdentity :: a }

instance Functor Identity where
    fmap f = Identity . f . runIdentity

 {- PROOF fmap (f . g) = fmap f . fmap g  (Functor 2)
    fmap f . fmap g
    ≡ (Identity . f . runIdentity) . (Identity . g . runIdentity)
    ≡ \x -> ((Identity . f . runIdentity) . (Identity . g . runIdentity)) x
    ≡ \x -> Identity $ f $ runIdentity $ Identity $ g $ runIdentity x
    ≡ \x -> Identity $ f $ runIdentity (Identity (g (runIdentity x)))
    ≡ \x -> Identity $ f $ (g (runIdentity x))
    ≡ \x -> Identity $ f $ g $ runIdentity x
    ≡ \x -> (Identity . f . g . runIdentity) x
    ≡ \x -> (Identity . (f . g) . runIdentity) x
    ≡ (Identity . (f . g) . runIdentity)
    ≡ fmap (f . g)
 -}

instance Applicative Identity where
    pure = Identity
    (Identity a) <*> (Identity b) = Identity (a b)

 {- PROOF composition
    pure (.) <*> u <*> v <*> w
    ≡ Identity (.) <*> Identity (runIdentity u) <*> Identity (runIdentity v) <*> Identity (runIdentity w)
    ≡ Identity ((.) (runIdentity u)) <*> Identity (runIdentity v) <*> Identity (runIdentity w)
    ≡ Identity ((.) (runIdentity u) (runIdentity v)) <*> Identity (runIdentity w)
    ≡ Identity (runIdentity u . runIdentity v) <*> Identity (runIdentity w)
    ≡ Identity ((runIdentity u . runIdentity v) (runIdentity w))
    ≡ Identity (runIdentity u (runIdentity v (runIdentity w)))
    ≡ Identity (runIdentity u) <*> (Identity (runIdentity v (runIdentity w)))
    ≡ u <*> (Identity (runIdentity v) <*> Identity (runIdentity w)
    ≡ u <*> (v <*> w)
 -}

instance Foldable Identity where
    foldr f z (Identity a) = f a z

 {- PROOF 2
    foldMap f
    ≡ foldr (mappend . f) mempty
    ≡ \(Identity a) -> foldr (mappend . f) mempty (Identity a)
    ≡ \(Identity a) -> (mappend . f) a mempty
    ≡ \(Identity a) -> (\e a -> mappend (f e) a) a mempty
    ≡ \(Identity a) -> mappend (f a) mempty
    ≡ \(Identity a) -> mappend (id (f a)) mempty
    ≡ \(Identity a) -> (mappend . id) (f a) mempty
    ≡ \(Identity a) -> foldr (mappend . id) mempty (Identity (f a))
    ≡ \(Identity a) -> foldr (mappend . id) mempty (fmap f (Identity a))
    ≡ \(Identity a) -> foldMap id (fmap f (Identity a))
    ≡ \(Identity a) -> (foldMap id . fmap f) (Identity a)
    ≡ foldMap id . fmap f
    ≡ fold . fmap f
 -}

instance Traversable Identity where
    traverse f (Identity a) = Identity <$> f a

 {- PROOF composition
    traverse (Compose . fmap g . f)
    ≡ \(Identity a) -> traverse (Compose . fmap g . f) (Identity a)
    ≡ \(Identity a) -> fmap Identity ((Compose . fmap g . f) a)
    ≡ \(Identity a) -> fmap Identity (Compose $ fmap g $ f a)
    ≡ \(Identity a) -> fmap Identity (Compose (fmap g $ f a))
    ≡ \(Identity a) -> Compose (fmap (fmap Identity) (fmap g (f a)))
    ≡ \(Identity a) -> Compose ((fmap (fmap Identity) . fmap g) (f a)))
    ≡ \(Identity a) -> Compose (fmap (fmap Identity . g) (f a))
    ≡ \(Identity a) -> Compose (fmap (\x -> (fmap Identity . g) x) (f a))
    ≡ \(Identity a) -> Compose (fmap (\x -> fmap Identity (g x)) (f a))
    ≡ \(Identity a) -> Compose (fmap (\x -> traverse g (Identity x)) (f a))
    ≡ \(Identity a) -> Compose (fmap (\x -> (traverse g . Identity) x) (f a))
    ≡ \(Identity a) -> Compose (fmap (traverse g . Identity) (f a))
    ≡ \(Identity a) -> Compose ((fmap (traverse g) . fmap Identity) (f a))
    ≡ \(Identity a) -> Compose ((fmap (traverse g) . fmap Identity) (f a))
    ≡ \(Identity a) -> Compose (fmap (traverse g) $ fmap Identity (f a))
    ≡ \(Identity a) -> Compose (fmap (traverse g) $ traverse f (Identity a))
    ≡ \(Identity a) -> (Compose . fmap (traverse g) . traverse f) (Identity a)
    ≡ Compose . fmap (traverse g) . traverse f
 -}

 -- Either

data Either a b = Left a | Right b

instance Functor (Either a) where
    fmap _ (Left e)  = Left e
    fmap f (Right e) = Right $ f e

 {- PROOF fmap id ≡ id (Functor 1)
    fmap id
    ≡ \x -> fmap id x
    1. x ≡ Right r
       ...
       ≡ \(Right r) -> fmap id (Right r)
       ≡ \(Right r) -> Right (id r)
       ≡ \(Right r) -> Right r
    2. x ≡ Left l
       ...
       ≡ \(Left l) -> fmap id (Left l)
       ≡ \(Left l) -> Left l
    ≡ id
 -}

instance Applicative (Either a) where
    pure = Right
    (Left l)  <*> _         = Left l
    _         <*> Left l    = Left l
    (Right a) <*> (Right b) = pure (a b)

 {- PROOF homomorphism
    pure f <*> pure x
    ≡ Right f <*> Right x
    ≡ Right (f x)
    ≡ pure (f x)
 -}

instance Foldable (Either a) where
    foldr _ z (Left _)  = z
    foldr f z (Right r) = f r z

 {- PROOF 1
    foldMap id ≡ fold      (def)
 -}

instance Traversable (Either a) where
    traverse _ (Left l)  = pure $ Left l
    traverse f (Right r) = Right <$> f r

 {- PROOF identity
    traverse Identity
    ≡ \x -> traverse Identity x
    1. x ≡ Left l
       ≡ \(Left l) -> traversable Identity (Left l)
       ≡ \(Left l) -> Identity (Left l)
    2. x ≡ Right r
       ≡ \(Right r) -> traversable Identity (Right r)
       ≡ \(Right r) -> fmap Right (Identity r)
       ≡ \(Right r) -> (Identity . Right . runIdentity) (Identity r)
       ≡ \(Right r) -> Identity $ Right $ runIdentity (Identity r)
       ≡ \(Right r) -> Identity (Right r)
    ≡ Identity
 -}

 -- Tree

data Tree a = Leaf | Node a (Tree a) (Tree a)

instance Functor Tree where
    fmap _ Leaf         = Leaf
    fmap f (Node a l r) = Node (f a) (fmap f l) (fmap f r)

 {- PROOF fmap id ≡ id (Functor 1)
    fmap id
    ≡ \t -> fmap id t
    1. t ≡ Leaf
       ≡ \Leaf -> fmap id Leaf
       ≡ \Leaf -> Leaf
    2. t ≡ Node e l r
       ≡ \(Node e l r) -> fmap id (Node e l r)
       ≡ \(Node e l r) -> Node (id e) (fmap id l) (fmap id r)
       ≡ \(Node e l r) -> Node e (induction) (induction)
       ≡ \(Node e l r) -> Node e l r
    ≡ id
 -}

instance Applicative Tree where
    pure x = Node x (pure x) (pure x)
    Leaf            <*> _               = Leaf
    _               <*> Leaf            = Leaf
    (Node le ll lr) <*> (Node re rl rr) = Node (le re) (ll <*> rl) (lr <*> rr)

 {- PROOF identity
    pure id <*> v
    ≡ Node id (pure id) (pure id) <*> v
    1. v ≡ Leaf
       ≡ Node id (pure id) (pure id) <*> Leaf
       ≡ Leaf
    2. v ≡ Node e l r
       ≡ Node id (pure id) (pure id) <*> Node e l r
       ≡ Node id (pure id) (pure id) <*> Node e l r
       ≡ Node (id e) (pure id <*> l) (pure id <*> r)
       (some induction)
       ≡ Node e l r
    ≡ v
 -}

instance Foldable Tree where
    foldr _ z Leaf         = z
    foldr f z (Node v l r) = foldr f (f v $ foldr f z r) l

 {- PROOF 1
    foldMap id ≡ fold (def)
 -}

instance Traversable Tree where
    traverse _ Leaf = pure Leaf
    traverse f (Node e l r) = go e l r
      where
        go v a b = (Node <$> f v) <*> traverse f a <*> traverse f b

 {- PROOF identity
    traverse Identity
    ≡ \x -> traverse Identity x
    1. x ≡ Leaf
       ≡ \Leaf -> pure Leaf
       ≡ \Leaf -> Identity Leaf
    2. x ≡ Node e l r
       ≡ \(Node e l r) -> go e l r
       ≡ \(Node e l r) -> (fmap Node (Identity e)) <*> traverse Identity l <*> traverse Identity r
       (induction)
       ≡ \(Node e l r) -> (Identity (Node e)) <*> Identity l <*> Identity r
       ≡ \(Node e l r) -> (Identity (Node e l)) <*> Identity r
       ≡ \(Node e l r) -> Identity (Node e l r)
    ≡ Identity
 -}

 -- Const

newtype Const a b = Const { getConst :: a }

instance Functor (Const a) where
    fmap _ (Const a) = Const a

 {- PROOF fmap (f . g) = fmap f . fmap g  (Functor 2)
    f :: a -> b
    g :: b -> c
    (f . g) :: a -> c
    fmap f . fmap g
    ≡ \(Const x :: Const q a) -> (fmap f . fmap g) (Const x :: Const q a)
    ≡ \(Const x :: Const q a) -> fmap f $ fmap g (Const x :: Const q a)
    ≡ \(Const x :: Const q a) -> fmap f $ (Const x :: Const q b)
    ≡ \(Const x :: Const q a) -> (Const x :: Const q c)
    ≡ \(Const x :: Const q a) -> fmap (f . g) (Const x :: Const q a)
    ≡ fmap (f . g)
 -}

instance Monoid a => Applicative (Const a) where
    pure _ = Const mempty
    (Const a) <*> (Const b) = Const $ mappend a b

 {- PROOF interchange
    u :: Const a b

    u <*> pure y
    ≡ (Const (getConst u) :: Const a b) <*> Const mempty
    ≡ (Const (getConst u) :: Const a b) <*> Const mempty
    ≡ Const $ mappend (getConst u) mempty
    ≡ Const $ (getConst u)
    ≡ Const $ mappend mempty (getConst u)
    ≡ Const mempty <*> (Const (getConst u) :: Const a b)
    ≡ pure ($ y) <*> u
 -}

instance Foldable (Const a) where
    foldr _ z (Const _) = z

 {- PROOF 1
    foldMap id ≡ fold (def)
 -}

instance Traversable (Const a) where
    traverse _ (Const a) = pure $ Const a

 {- PROOF identity
    traverse Identity
    ≡ \(Const x :: Const a b) -> traverse Identity (Const x :: Const a b)
    ≡ \(Const x :: Const a b) -> pure (Const x :: Const a b)
    ≡ \(Const x :: Const a b) -> Identity (Const x :: Const a b)
    ≡ Identity
 -}

 -- (a,b)

newtype P a b = P (a,b)

instance Functor (P a) where
    fmap f (P (a,b)) = P (a, f b)

 {- PROOF fmap id ≡ id (Functor 1)
    fmap id
    ≡ \(P (a,b)) -> fmap id (P (a,b))
    ≡ \(P (a,b)) -> P (a, id b)
    ≡ \(P (a,b)) -> P (a,b)
    ≡ id
 -}

instance Monoid a => Applicative (P a) where
    pure x = P (mempty,x)
    (P (a,b)) <*> (P (c,d)) = P (mappend a c, b d)

 {- PROOF identity
    pure id <*> v
    ≡ P (mempty,id) <*> P (a,b)
    ≡ P (mappend mempty a, id b)
    ≡ P (a,b)
    ≡ v
 -}

instance Foldable (P a) where
    foldr f z (P (_,x)) = f x z

 {- PROOF 1
    foldMap id ≡ fold (def)
 -}

instance Traversable (P a) where
    traverse f (P (a, b)) = (\x -> P (a,x)) <$> f b

 {- PROOF naturality
    t . traverse f
    ≡ \(P (a,b)) -> (t . traverse f) (P (a,b))
    ≡ \(P (a,b)) -> t $ traverse f (P (a,b))
    ≡ \(P (a,b)) -> t $ fmap (\x -> (P (a,x))) (f b)
    ≡ \(P (a,b)) -> t $ pure (\x -> (P (a,x))) <*> (f b)
    ≡ \(P (a,b)) -> t (pure (\x -> (P (a,x)))) <*> t (f b)
    ≡ \(P (a,b)) -> pure (\x -> (P (a,x))) <*> t (f b)
    ≡ \(P (a,b)) -> fmap (\x -> (P (a,x))) (t (f b))
    ≡ \(P (a,b)) -> fmap (\x -> (P (a,x))) ((t . f) b)
    ≡ traverse (t . f)
 -}
