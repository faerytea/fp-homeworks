{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE NoImplicitPrelude    #-}
{-# LANGUAGE UndecidableInstances #-}

module BasicTypeClasses.MonadJoin where

import           BasicTypeClasses.Classes (Functor (fmap), Monad (return, (>>=)),
                                           MonadFish (returnFish, (>=>)),
                                           MonadJoin (join, returnJoin), id)

instance (Functor m, MonadJoin m) => Monad m where
    return = returnJoin
    (>>=) a f = join (fmap f a)

    {- PROOF m >>= return ≡ m
       m >>= return
       ≡ join (fmap return m)
       ≡ (join . fmap return) m
       ≡ (join . fmap returnJoin) m
       ≡ id m
       ≡ m
    -}

instance (Functor m, MonadJoin m) => MonadFish m where
    returnFish = returnJoin
    (>=>) f g = \x -> join (fmap g (f x))

    {- PROOF f >=> returnFish ≡ f
       f >=> returnFish
       ≡ \x -> join (fmap returnFish (f x))
       ≡ \x -> (join . fmap returnJoin) (f x)
       ≡ \x -> id (f x)
       ≡ \x -> f x
       ≡ f
    -}
