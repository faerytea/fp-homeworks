{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards   #-}

module Monstupar
       ( Monstupar
       , runParser
       , ParseError
       , peExpected
       , peInput
       , satisfy
       , exactly
       , posInt
       , abParser
       , abParser_
       , intPair
       , intOrUppercase
       , zeroOrMore
       , oneOrMore
       , spaces
       , (?|)
       ) where

import           Control.Applicative (Alternative (..))
import           Control.Monad       (void, (>=>))
import           Data.Bifunctor      (first, second)
import           Data.Char           (isDigit, isSpace, isUpper)
import           Data.Tuple          (swap)

newtype Monstupar s a = Monstupar { runParser :: [s] -> Either (ParseError s) ([s], a) }

data ParseError s = ExpectedButGot
                  { peExpected :: String
                  , peInput    :: [s] }
                  | ErrorMessage String [s]
                  deriving Eq

instance Show s => Show (ParseError s) where
    show ExpectedButGot {..} = "expected " ++ peExpected ++ ", but got " ++ show peInput
    show (ErrorMessage message input) = "error: " ++ message ++ " on `" ++ show input ++ "'"

satisfy :: (a -> Bool) -> Monstupar a a
satisfy p = Monstupar f
  where
    f [] = Left $ ErrorMessage "unexpected finish" []
    f (e:ls) = if p e
               then Right (ls, e)
               else Left $ ErrorMessage "not satisfies" (e:ls)

exactly :: Eq a => a -> Monstupar a a
exactly c = satisfy (== c)

posInt :: Monstupar Char Integer
posInt = Monstupar f
  where
    f s = let p@(n, _) = span isDigit s in
          if null n
          then Left $ "number" `ExpectedButGot` s
          else Right $ swap $ first (read :: String -> Integer) p

instance Functor (Monstupar s) where
    fmap f (Monstupar p) = Monstupar (fmap (second f) . p)

instance Applicative (Monstupar s) where
    pure a = Monstupar $ \s -> Right (s,a)
    (<*>) p1 p2 = Monstupar $ runParser p1 >=> (\(r, f) -> (second f <$> runParser p2 r))

abParser :: Monstupar Char (Char, Char)
abParser = (\l r -> (l,r)) <$> exactly 'a' <*> exactly 'b'

abParser_ :: Monstupar Char ()
abParser_ = void abParser

intPair :: Monstupar Char [Integer]
intPair = (\l _ r -> [l,r]) <$> posInt <*> exactly ' ' <*> posInt

instance Alternative (Either (ParseError s)) where
    empty = Left $ ErrorMessage "unknown error, input unavailable" []
    (Left _) <|> r@(Right _) = r
    a <|> _ = a

instance Alternative (Monstupar s) where
    empty = Monstupar $ const empty
    (<|>) p1 p2 = Monstupar $ \s -> runParser p1 s <|> runParser p2 s

intOrUppercase :: Monstupar Char ()
intOrUppercase = void posInt <|> void (satisfy isUpper)

zeroOrMore :: Monstupar a b -> Monstupar a [b]
zeroOrMore p = foldr (\a b -> ((:) <$> a <*> b) <|> pure []) (pure []) $ repeat p

oneOrMore  :: Monstupar a b -> Monstupar a [b]
oneOrMore p = (:) <$> p <*> zeroOrMore p

spaces :: Monstupar Char String
spaces = zeroOrMore $ satisfy isSpace

(?|) :: Monstupar s a -> a -> Monstupar s a
(?|) p d = p <|> pure d

instance Monad (Monstupar s) where
    (>>=) p f = Monstupar (runParser p >=> (\(rest, res) -> runParser (f res) rest))
    fail s = Monstupar $ const $ Left $ ErrorMessage s []

 {- PROOF Functor (Monstupar s)
    fmap id ≡ id
    fmap id
    ≡ \(Monstupar f) -> fmap id (Monstupar f)
    ≡ \(Monstupar f) -> Monstupar (fmap (second id) . f)
    ≡ \(Monstupar f) -> Monstupar (fmap id . f)
    ≡ \(Monstupar f) -> Monstupar (id . f)
    ≡ \(Monstupar f) -> Monstupar f
    ≡ id

    PROOF Applicative (Monstupar s)
    pure f <*> pure x
    ≡ (Monstupar $ \s -> Right (s,f)) <*> (Monstupar $ \s -> Right (s,x))
    ≡ Monstupar $ runParser (Monstupar $ \s -> Right (s,f))
      >=> (\(r, f) -> (second f <$> runParser (Monstupar $ \s -> Right (s,x)) r))
    ≡ Monstupar $ (\s -> Right (s,f))
      >=> (\(r, f) -> (second f <$> (\s -> Right (s,x)) r))
    ≡ Monstupar $ (\s -> Right (s,f)) >=> (\(r, f) -> (second f <$> Right (r,x)))
    ≡ Monstupar $ (\s -> Right (s,f)) >=> (\(r, f) -> Right (second f (r,x)))
    ≡ Monstupar $ (\s -> Right (s,f)) >=> (\(r, f) -> Right (r, f x))
    ≡ Monstupar $ \y -> (\s -> Right (s,f)) y >>= (\(r, f) -> Right (r, f x))
    ≡ Monstupar $ \y -> Right (y,f) >>= (\(r, f) -> Right (r, f x))
    ≡ Monstupar $ \y -> (\(r, f) -> Right (r, f x)) (y,f)
    ≡ Monstupar $ \y -> Right (y, f x)
    ≡ Monstupar $ \s -> Right (s, f x)
    ≡ pure (f x)

    PROOF Monad (Monstupar s)
    return a >>= k
    ≡ pure a >>= k
    ≡ (Monstupar $ \s -> Right (s,a)) >>= k
    ≡ Monstupar (runParser (Monstupar $ \s -> Right (s,a))
      >=> (\(rest, res) -> runParser (k res) rest))
    ≡ Monstupar ((\s -> Right (s,a)) >=> (\(rest, res) -> runParser (k res) rest))
    ≡ Monstupar (\x -> (\s -> Right (s,a)) x >>= (\(rest, res) -> runParser (k res) rest))
    ≡ Monstupar (\x -> Right (x,a) >>= (\(rest, res) -> runParser (k res) rest))
    ≡ Monstupar (\x -> (\(rest, res) -> runParser (k res) rest) (x,a))
    ≡ Monstupar (\x -> runParser (k a) x)
    ≡ Monstupar (runParser (k a))
    ≡ k a
 -}