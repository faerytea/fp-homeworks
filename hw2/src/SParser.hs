module SParser
       ( Parser
       , runParser
       , zeroOrMore
       , oneOrMore
       , spaces
       , spaces1
       , ident
       , Ident
       , Atom(N, I)
       , SExpr(A, Comb)
       , parserSExpr
       ) where

import           Control.Applicative (Alternative (..))

import           Data.Char           (isAlpha, isAlphaNum, isSpace)

import           AParser             (Parser, char, posInt, runParser, satisfy)

zeroOrMore :: Parser a -> Parser [a]
zeroOrMore p = foldr (\a b -> ((:) <$> a <*> b) <|> pure []) (pure []) $ repeat p

oneOrMore  :: Parser a -> Parser [a]
oneOrMore p = (:) <$> p <*> zeroOrMore p

spaces :: Parser String
spaces = zeroOrMore $ satisfy isSpace

spaces1 :: Parser String
spaces1 = oneOrMore $ satisfy isSpace

ident :: Parser String
ident = (:) <$> satisfy isAlpha <*> zeroOrMore (satisfy isAlphaNum)

type Ident = String

data Atom = N Integer
          | I Ident
          deriving (Show, Eq)

data SExpr = A Atom
           | Comb [SExpr]
           deriving (Show, Eq)

atom :: Parser Atom
atom = (I <$> ident) <|> (N <$> posInt)

parserSExpr :: Parser SExpr
parserSExpr = spaces
            *> (Comb <$> (char '('
            *> spaces *> ((:) <$> parserSExpr) <*> zeroOrMore (spaces1 *> parserSExpr)
            <* spaces <* char ')')
            <|> A <$> atom)
            <* spaces
