module ParserM
       ( Parser
       , runParser
       , parserLetProgram
       , LetProgram(LetProgram)
       , LetExpr(LetExpr)
       , Expr(Expr)
       , Atom(N, I)
       , Ident
       , optimizeLet
       ) where

import           Control.Applicative (Alternative (..))

import           Data.Bifunctor      (first, second)
import           Data.Coerce         (coerce)
import           Data.List           (intercalate, partition)
import qualified Data.Map.Strict     as DM (Map, findWithDefault, fromList, toList, union)

import           AParser             (Parser, char, posInt, runParser)
import           SParser             (Ident, ident, spaces, spaces1, zeroOrMore)

newtype LetProgram = LetProgram [LetExpr]

data LetExpr = LetExpr Ident Expr

newtype Expr = Expr [Atom]

data Atom = N Integer
          | I Ident
          deriving Eq

instance Show Atom where
    show (N i) = show i
    show (I i) = i

instance Show Expr where
    show (Expr lst) = intercalate " + " $ map show lst

instance Show LetExpr where
    show (LetExpr i e) = "let " ++ i ++ " = " ++ show e

instance Show LetProgram where
    show (LetProgram p) = intercalate "\n" $ map show p

(?|) :: Parser a -> a -> Parser a
(?|) p d = p <|> pure d

sign :: Parser (Integer -> Integer)
sign = (\x -> if x == '+' then (1 *) else ((-1 :: Integer) *)) <$> (char '+' <|> char '-')

int :: Parser Integer
int = (sign ?| (1*)) <*> posInt

except :: Parser a -> (a -> Bool) -> Parser a
except parser predicate = parser >>= (\r -> if predicate r then empty else pure r)

atom :: Parser Atom
atom = (I <$> (ident `except` (== "let"))) <|> (N <$> int)

expr :: Parser Expr
expr = Expr <$>
       (spaces *> ((:) <$> atom) <*> zeroOrMore (spaces *> char '+' *> spaces *> atom))

word :: String -> Parser String
word = foldr (\c p -> ((:) <$> char c) <*> p) (pure "")

parserLetExpr :: Parser LetExpr
parserLetExpr = spaces *> word "let" *> spaces1 *>
                (LetExpr <$> ident) <* spaces <* char '=' <*> expr

parserLetProgram :: Parser LetProgram
parserLetProgram = LetProgram <$> ((:) <$> parserLetExpr <*> zeroOrMore (spaces1 *> parserLetExpr))

optimizeLet :: LetProgram -> Maybe LetProgram
optimizeLet (LetProgram p) = LetProgram <$> optimize p
  where
    foldConst :: Expr -> Expr
    foldConst (Expr lst) = Expr (foldConstUnwrapped lst)
      where
        foldConstUnwrapped :: [Atom] -> [Atom]
        foldConstUnwrapped = foldr foldHelper []
        foldHelper :: Atom -> [Atom] -> [Atom]
        foldHelper (N c) (N n:ls) = N (n + c) : ls
        foldHelper (I c) (N n:ls) = N n : I c : ls
        foldHelper a ls           = a : ls
    initMap :: [LetExpr] -> (DM.Map Ident Atom, [(Ident, Expr)])
    initMap = makeMap . map (\(LetExpr i e) -> (i,e))
    makeMap :: [(Ident, Expr)] -> (DM.Map Ident Atom, [(Ident, Expr)])
    makeMap exprs = first (DM.fromList . map (second (\(Expr [N n]) -> N n)))
                  $ partition (folded . coerce . snd)
                  $ map (second foldConst) exprs
    folded :: [Atom] -> Bool
    folded [N _] = True
    folded _     = False
    rewrite :: (DM.Map Ident Atom, [(Ident, Expr)]) -> [(Ident, Expr)]
    rewrite (m, t) = map (\(i, Expr as) -> (i, Expr $ map rwHelper as)) t
      where
        rwHelper :: Atom -> Atom
        rwHelper (I i) = DM.findWithDefault (I i) i m
        rwHelper (N n) = N n
    optimizeStep :: (DM.Map Ident Atom, [(Ident, Expr)], Bool) -> (DM.Map Ident Atom, [(Ident, Expr)], Bool)
    optimizeStep (m, t, _) = let rwn = rewrite (m, t)
                                 (nm, nt) = makeMap rwn
                             in (DM.union m nm, nt, null nm)
    wrapBack :: DM.Map Ident Atom -> [LetExpr]
    wrapBack = map (\(i, a) -> LetExpr i (Expr [a])) . DM.toList
    optimize :: [LetExpr] -> Maybe [LetExpr]
    optimize les = wrapBack
                 <$> case head
                        $ dropWhile (\(_, t, ff) -> not (null t) && not ff)
                        $ iterate optimizeStep ((\(a,b) -> (a,b,False)) $ initMap les) of
                          (m, [], _) -> Just m
                          (_, _, _)  -> Nothing
