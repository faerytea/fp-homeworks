module AParser
       ( Parser
       , runParser
       , satisfy
       , char
       , posInt
       , abParser
       , abParser_
       , intPair
       , intOrUppercase
       , finalParser
       ) where

import           Control.Applicative (Alternative (..))
import           Control.Monad       (void, (>=>))

import           Data.Bifunctor      (first)
import           Data.Char           (isDigit, isUpper)

newtype Parser a = Parser { runParser :: String -> Maybe (a, String) }

satisfy :: (Char -> Bool) -> Parser Char
satisfy p = Parser f
  where
    f [] = Nothing
    f (e:ls) = if p e
               then Just (e, ls)
               else Nothing

char :: Char -> Parser Char
char c = satisfy (c ==)

posInt :: Parser Integer
posInt = Parser f
  where
    f s = let p@(n, _) = span isDigit s in
          if null n
          then Nothing
          else Just $ first (read :: String -> Integer) p

instance Functor Parser where
    fmap f (Parser p) = Parser (fmap (first f) . p)

instance Applicative Parser where
    pure a = Parser $ \s -> Just (a,s)
    (<*>) p1 p2 = Parser $ runParser p1 >=> (\(f, r) -> (first f <$> runParser p2 r))

abParser :: Parser (Char, Char)
abParser = (\l r -> (l,r)) <$> char 'a' <*> char 'b'

abParser_ :: Parser ()
abParser_ = void abParser

intPair :: Parser [Integer]
intPair = (\l _ r -> [l,r]) <$> posInt <*> char ' ' <*> posInt

instance Alternative Parser where
    empty = Parser $ const Nothing
    (<|>) p1 p2 = Parser $ \s -> runParser p1 s <|> runParser p2 s

intOrUppercase :: Parser ()
intOrUppercase = void posInt <|> void (satisfy isUpper)

instance Monad Parser where
    (>>=) p f = Parser (runParser p >=> (\(res, rest) -> runParser (f res) rest))

finalParser :: Parser a -> Parser a
finalParser p = Parser $ runParser p >=> (\(a,rest) -> if null rest then Just (a,[]) else Nothing)

{- PROOF Functor
   fmap id ≡ id
   fmap id (Parser p) ≡ id (Parser p)
   Parser (fmap (first id) . p) ≡ Parser p
   Parser (fmap id . p) ≡ Parser p
   Parser (id . p) ≡ Parser p
   Parser p ≡ Parser p
-}

{- PROOF Applicative identity
   pure id <*> v ≡ v
   Parser (\s -> Just (id, s)) <*> v ≡ v
   Parser $ (\s -> Just (id, s)) >=> (\(f,r) -> (first f <$> runParser v r)) ≡ v
   Parser $ \x -> (\s -> Just (id,s)) x >>= (\(f,r) -> (first f <$> runParser v r)) ≡ v
   Parser $ \x -> Just (id,x) >>= (\(f,r) -> (first f <$> runParser v r)) ≡ v
   Parser $ \x -> (\(f,r) -> (first f <$> runParser v r)) (id,x) ≡ v
   Parser $ \x -> ((first id <$> runParser v x)) ≡ v
   Parser $ \x -> ((id <$> runParser v x)) ≡ v
   Parser $ \x -> ((id $ runParser v x)) ≡ v
   Parser $ \x -> runParser v x ≡ v
   Parser $ runParser v ≡ v
   v ≡ v
-}

{- PROOF Monad m >>= return ≡ m
   m >>= return ≡ m
   Parser (runParser m >=> (\(res, rest) -> runParser (return res) rest)) ≡ m
   Parser (runParser m >=> (\(res, rest) -> runParser (Parser $ \s -> Just (res,s)) rest)) ≡ m
   Parser (runParser m >=> (\(res, rest) -> (\s -> Just (res,s)) rest)) ≡ m
   Parser (runParser m >=> (\(res, rest) -> Just (res,rest))) ≡ m
   Parser (runParser m >=> Just) ≡ m
   Parser (\x -> runParser m x >>= Just) ≡ m
   runParser (Parser (\x -> runParser m x >>= Just)) text ≡ runParser m text
   (\x -> runParser m x >>= Just) text ≡ runParser m text
   runParser m text >>= Just ≡ runParser m text
   1. runParser m text ≡ Nothing
      Nothing >>= Just ≡ Nothing
      Nothing ≡ Nothing
   2. runParser m text ≡ Just (a,r)
      Just (a,r) >>= Just ≡ Just (a,r)
      Just (a,r) ≡ Just (a,r)
-}
