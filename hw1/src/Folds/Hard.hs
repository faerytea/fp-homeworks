module Folds.Hard
       ( splitOn
       , joinWith
       ) where

splitOn :: Char -> String -> [String]
splitOn sym = foldr magic [[]]
  where
    magic :: Char -> [String] -> [String]
    magic s (l:ls) = if sym == s
                     then []:(l:ls)
                     else (s:l):ls
    magic _ _ = undefined

joinWith :: Char -> [String] -> String
joinWith _ []        = ""
joinWith _ [f]       = f
joinWith c (f:s:lst) = concat $ f : map (c :) (s : lst)
