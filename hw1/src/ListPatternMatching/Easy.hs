module ListPatternMatching.Easy
       ( removeAt
       , collectEvery
       , stringSum
       , mergeSort
       ) where

import           Prelude

import           ListPatternMatching.Hard (collectEvery, mergeSort)
import qualified ListPatternMatching.Hard as H (removeAt)

stringSum :: String -> Integer
stringSum = sum . map read . words

removeAt :: Int -> [a] -> [a]
removeAt pos lst = snd $ H.removeAt pos lst
