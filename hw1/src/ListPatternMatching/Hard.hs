module ListPatternMatching.Hard
       ( removeAt
       , collectEvery
       , stringSum
       , mergeSort
       ) where

import           Prelude

import           Data.Char (isDigit)

removeAt :: Int -> [a] -> (Maybe a, [a])
removeAt 0 (e:ls) = (Just e,ls)
removeAt p (e:ls) = mapSnd ((:) e) (removeAt (p-1) ls)
removeAt _ lst    = (Nothing, lst)

collectEvery :: Int -> [a] -> ([a], [a])
collectEvery n l = mapHomoPair reverse $ ceInternal ([], []) 1 n l
  where
    ceInternal :: ([a], [a]) -> Int -> Int -> [a] -> ([a], [a])
    ceInternal (others, selected) _ _ [] = (others, selected)
    ceInternal (others, selected) c k (e:lst)
      | c == k = ceInternal (others, e : selected) 1 k lst
      | otherwise = ceInternal (e : others, selected) (c + 1) k lst

stringSum :: String -> Integer
stringSum = sum . map convert . words
  where
    convert :: String -> Integer
    convert ('+':n@(d:_))
      | isDigit d = read n
      | otherwise = error "no parse"
    convert n = read n

mergeSort :: Ord a => [a] -> [a]
mergeSort [] = []
mergeSort [e] = [e]
mergeSort lst = merge $ mapHomoPair mergeSort $ collectEvery 2 lst
  where
    merge :: Ord a => ([a], [a]) -> [a]
    merge (l, []) = l
    merge ([], r) = r
    merge (l:ls, r:rs) =
      if l < r
        then l : merge (ls, r : rs)
        else r : merge (l : ls, rs)

mapSnd :: (b -> c) -> (a, b) -> (a, c)
mapSnd f (a, b) = (a, f b)

mapPair :: (a -> c, b -> d) -> (a, b) -> (c, d)
mapPair (f, g) (a, b) = (f a, g b)

mapHomoPair :: (a -> b) -> (a, a) -> (b, b)
mapHomoPair f = mapPair (f, f)
