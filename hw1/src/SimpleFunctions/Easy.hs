module SimpleFunctions.Easy
       ( order3
       , highestBit
       , smartReplicate
       , contains
       ) where

import           Prelude

import           SimpleFunctions.Hard (contains, order3, smartReplicate)
import qualified SimpleFunctions.Hard as H (highestBit)

highestBit :: Integral a => a -> Integer
highestBit = fst . H.highestBit
