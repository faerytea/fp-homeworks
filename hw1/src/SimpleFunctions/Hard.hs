module SimpleFunctions.Hard
       ( order3
       , highestBit
       , smartReplicate
       , contains
       ) where

import           Prelude

order3 :: Ord a => (a, a, a) -> (a, a, a)
order3 = orderFirst2 . orderLast2 . orderFirst2
  where
    orderLast2  t@(a, b, c) = if b > c then (a, c, b) else t
    orderFirst2 t@(a, b, c) = if a > b then (b, a, c) else t

highestBit :: Integral a => a -> (Integer, Integer)
highestBit a
    | a <= 0    = error "there is no general way to get highest bit of negative integral number"
    | otherwise = (2 ^ pow, pow)
      where
        pow :: Integer
        pow = head $ take 1 $ filter (\x -> 2^x > (a `div` 2)) [0..]

smartReplicate :: [Int] -> [Int]
smartReplicate = concatMap (\x -> replicate x x)

contains :: Eq a => a -> [[a]] -> [[a]]
contains e = filter (elem e)
