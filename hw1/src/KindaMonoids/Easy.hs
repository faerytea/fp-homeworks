module KindaMonoids.Easy
       ( maybeConcat
       , NonEmpty(..)
       , Identity(..)
       ) where

import           KindaMonoids.Hard
