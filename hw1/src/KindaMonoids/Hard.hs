{-# LANGUAGE ScopedTypeVariables #-}

module KindaMonoids.Hard
       ( maybeConcat
       , eitherConcat
       , NonEmpty(..)
       , Identity(..)
       , Name
       , Endo(..)
       , Arrow(..)
       ) where

import           Data.Coerce        (coerce)
import           Data.Either        (lefts, rights)
import           Data.List          (intercalate)
import qualified Data.List.NonEmpty as DLN (NonEmpty ((:|)))
import           Data.Maybe         (fromMaybe)
import           Data.Semigroup     (Semigroup (..))

maybeConcat :: [Maybe [a]] -> [a]
maybeConcat = fromMaybe [] . mconcat

eitherConcat :: (Monoid a, Monoid b) => [Either a b] -> (a, b)
eitherConcat lst = (mconcat $ lefts lst, mconcat $ rights lst)

data NonEmpty a = a :| [a] deriving (Show)

instance Semigroup (NonEmpty a) where
  (ha :| la) <> (hb :| lb) = ha :| (la ++ hb:lb)

newtype Identity a = Identity { runIdentity :: a }

instance Monoid a => Monoid (Identity a) where
  mempty = Identity mempty
  mappend (Identity a) (Identity b) = Identity $ mappend a b
  mconcat = Identity . mconcat . coerce

instance Semigroup a => Semigroup (Identity a) where
  (Identity a) <> (Identity b) = Identity $ a <> b
  sconcat = Identity . sconcat . coerce
  stimes t (Identity i) = Identity $ stimes t i

newtype Name = Name String

instance Monoid Name where
  mempty = Name ""
  mappend (Name a) (Name b) = Name $ a ++ '.':b
  mconcat = Name . intercalate "." . coerce

instance Semigroup Name where
  (<>) = mappend
  sconcat = mconcat . (\(h DLN.:| t) -> h:t)
  stimes t = Name . intercalate "." . replicate (fromIntegral t) . coerce

newtype Endo a = Endo { getEndo :: a -> a }

instance Monoid (Endo a) where
  mempty = Endo id
  mappend (Endo f) (Endo g) = Endo $ g . f
  mconcat = Endo . foldr (flip (.)) id . (coerce :: [Endo a] -> [a -> a])

instance Semigroup (Endo a) where
  (<>) = mappend
  sconcat = mconcat . (\(h DLN.:| t) -> h:t)
  stimes t = mconcat . replicate (fromIntegral t)

newtype Arrow a b = Arrow { getArrow :: a -> b }

instance Monoid    b => Monoid    (Arrow a b) where
  mempty = Arrow $ const mempty
  mappend (Arrow f) (Arrow g) = Arrow $ \a -> mappend (f a) (g a)

instance Semigroup b => Semigroup (Arrow a b) where
  (Arrow f) <> (Arrow g) = Arrow $ \a -> f a <> g a

