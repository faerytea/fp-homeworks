module Tree
       ( Tree(Leaf, Node)
       , height
       , find
       , insert
       , fromList
       , value
       ) where

import           Data.Foldable  (foldl')
import           Data.Maybe     (fromJust)
import           Data.Semigroup (Semigroup (..))

data Tree a = Leaf | Node a (Tree a) (Tree a) deriving Show

height :: Tree a -> Int
height Leaf         = 0
height (Node _ l r) = 1 + max (height l) (height r)

find :: Ord a => Tree a -> a -> Tree a
find Leaf _ = Leaf
find node@(Node v l r) e
    | e == v    = node
    | e < v     = findDirect l (<=)
    | otherwise = findDirect r (>=)
  where
    findDirect n op = case value n of
        Nothing -> node
        (Just _)  -> if op v $ fromJust $ value node
                     then find n e
                     else node

value :: Tree a -> Maybe a
value Leaf         = Nothing
value (Node v _ _) = Just v

insert :: Ord a => Tree a -> a -> Tree a
insert Leaf v = Node v Leaf Leaf
insert (Node v l r) e
    | e < v = Node v (insert l e) r
    | e > v = Node v l (insert r e)
    | otherwise = Node e l (Node v Leaf r)

fromList :: Ord a => [a] -> Tree a
fromList = foldl' insert Leaf

instance Foldable Tree where
  foldr _ z Leaf         = z
  foldr f z (Node v l r) = foldr f (f v $ foldr f z r) l
  foldMap _ Leaf         = mempty
  foldMap f (Node v l r) = mconcat [foldMap f l, f v, foldMap f r]

instance Ord a => Monoid (Tree a) where
  mempty = Leaf
  mappend = foldr $ flip insert

instance Ord a => Semigroup (Tree a) where
  (<>) = mappend
