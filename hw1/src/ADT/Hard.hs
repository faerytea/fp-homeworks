module ADT.Hard
       ( DayOfWeek(..)
       , nextDay
       , afterDays
       , isWeekend
       , daysToParty
       , Fighter(..)
       , HolyKnight(..)
       , EvilBeast(..)
       , UnknownSlime(..)
       , Attacker(..)
       , attacksLeft
       , attacksRight
       , chooseFirst
       , switchPositions
       , attack
       , round
       , FightResult(..)
       , fight
       , Vector(Vector2D, Vector3D)
       , length
       , vecSum
       , scalarMult
       , distVec
       , vecMult
       , Nat(S, Z)
       , evenNat
       , oddNat
       ) where

import           Prelude     hiding (length, round)

import           Data.Coerce (coerce)
import           Data.Maybe  ()
import           Data.Ratio  ((%))

data DayOfWeek = Monday
               | Tuesday
               | Middle
               | Forth
               | Friday
               | Saturday
               | Sunday deriving (Eq, Ord, Show, Read, Enum, Bounded)

daysInWeek :: Int
daysInWeek = fromEnum (maxBound :: DayOfWeek) - fromEnum (minBound :: DayOfWeek) + 1

nextDay :: DayOfWeek -> DayOfWeek
nextDay day
    | day == maxBound = minBound
    | otherwise       = succ day

afterDays :: Int -> DayOfWeek -> DayOfWeek
afterDays after day = head $ drop (after `mod` daysInWeek) (iterate nextDay day)

isWeekend :: DayOfWeek -> Bool
isWeekend = (> Friday)

daysToParty :: DayOfWeek -> Int
daysToParty day = let iDay    = fromEnum day
                      iFriday = fromEnum Friday
                      shifted = if iDay > iFriday then iDay + daysInWeek else iDay
                  in shifted - iFriday

class Fighter a where
    name :: a -> String
    morale :: a -> Int
    damage :: a -> Int
    health :: a -> Int
    hit :: a -> Int -> a
    alive :: a -> Bool
    alive f = health f > 0

data HolyKnight = HolyKnight
                  { hkName   :: String
                  , hkDamage :: Int
                  , hkHealth :: Int
                  } deriving Show

instance Fighter HolyKnight where
  name = hkName
  morale _ = maxBound
  damage = hkDamage
  health = hkHealth
  hit knight d = HolyKnight (hkName knight) (hkDamage knight) (hkHealth knight - d)

data EvilBeast = EvilBeast
                 { ebName   :: String
                 , ebDamage :: Int
                 , ebHealth :: Int
                 } deriving Show

instance Fighter EvilBeast where
  name = ebName
  morale _ = 0
  damage = ebDamage
  health = ebHealth
  hit beast d = EvilBeast (ebName beast) (ebDamage beast) (ebHealth beast - d)

data UnknownSlime = UnknownSlime
                    { usColor :: String
                    , usSize  :: Int
                    } deriving Show

instance Fighter UnknownSlime where
  name slime = "the " ++ usColor slime ++ " slime"
  morale _ = minBound
  damage = usSize
  health = usSize
  hit slime d = UnknownSlime (usColor slime) (usSize slime - d)

newtype Attacker = Attacker Bool

instance Show Attacker where
  show (Attacker True)  = "left"
  show (Attacker False) = "right"

attacksLeft :: Attacker
attacksLeft = Attacker True

attacksRight :: Attacker
attacksRight = Attacker False

chooseFirst :: (Fighter l, Fighter r) => l -> r -> Attacker
chooseFirst f s = coerce $ morale f >= morale s

switchPositions :: Attacker -> Attacker
switchPositions = coerce . not . coerce

attack :: (Fighter a, Fighter d) => a -> d -> d
attack a d = hit d (damage a)

round :: (Fighter a, Fighter d) => (a, d) -> Attacker -> (a, d)
round (a, d) (Attacker True)  = (a, hit d $ damage a)
round (d, a) (Attacker False) = (hit d $ damage a, a)

data FightResult l r = FightResult
                   { frStrikes :: Int
                   , frLeft    :: l
                   , frRight   :: r
                   , frLeftWon :: Bool }

instance (Fighter l, Fighter r) => Show (FightResult l r) where
  show (FightResult s l r winnerLeft)
      = "The epic battle lasts " ++ show s ++ " strikes; and finally "
        ++ winnerName ++ " defeated " ++ loserName ++ ". " ++ " After the battle, "
        ++ winnerName ++ " have only " ++ hp ++ " hp. "
    where
      (winnerName, loserName) = if winnerLeft
                                then (name l, name r)
                                else (name r, name l)
      hp = show $ if winnerLeft
                  then health l
                  else health r

fight :: (Fighter l, Fighter r) => (l, r) -> FightResult l r
fight fighters@(l, r) =
  toResult $ head $ dropWhile (\(fs, _, _) -> bothAlive fs) $ iterate fightRound (fighters, priority, 0)
  where
    priority :: Attacker
    priority = chooseFirst l r
    bothAlive :: (Fighter f, Fighter s) => (f, s) -> Bool
    bothAlive (f1, f2) = alive f1 && alive f2
    fightRound (fts, attacker, roundNo) = (round fts attacker, switchPositions attacker, roundNo + 1)
    toResult ((f1, f2), _, rNo) = FightResult rNo f1 f2 $ alive f1

data Vector a = Vector2D a a
              | Vector3D a a a deriving (Show, Eq)

toList :: Vector a -> [a]
toList (Vector2D x y)   = [x,y]
toList (Vector3D x y z) = [x,y,z]

vecFromList :: [a] -> Vector a
vecFromList [x,y]   = Vector2D x y
vecFromList [x,y,z] = Vector3D x y z
vecFromList _       = error "Wrong usage of internal function"

length :: Real a => Vector a -> Double
length = sqrt . sum . map ((^ (2 :: Integer)) . realToFrac) . toList

vecMap :: a -> (a -> a -> a) -> Vector a -> Vector a -> Vector a
vecMap z f a b = vecFromList $ uncurry (zipWith f) $ mapHomoPair toList $ sameDim z a b

mapPair :: (a -> c, b -> d) -> (a, b) -> (c, d)
mapPair (f, g) (a, b) = (f a, g b)

mapHomoPair :: (a -> b) -> (a, a) -> (b, b)
mapHomoPair f = mapPair (f, f)

vecSum :: Num a => Vector a -> Vector a -> Vector a
vecSum = vecMap 0 (+)

scalarMult :: Num a => Vector a -> Vector a -> Vector a
scalarMult = vecMap 0 (*)

sameDim :: a -> Vector a -> Vector a -> (Vector a, Vector a)
sameDim _ v1@Vector2D {} v2@Vector2D {} = (v1, v2)
sameDim _ v1@Vector3D {} v2@Vector3D {} = (v1, v2)
sameDim z v1@Vector3D {} (Vector2D x y) = (v1, Vector3D x y z)
sameDim z (Vector2D x y) v2@Vector3D {} = (Vector3D x y z, v2)

distVec :: Real a => Vector a -> Vector a -> Double
distVec a b = length $ vecMap 0 (-) a b

vecMult :: Num a => Vector a -> Vector a -> Vector a
vecMult (Vector3D ax ay az) (Vector3D bx by bz) =
    Vector3D (ay * bz - az * by) (az * bx - ax * bz) (ax * by - ay * bx)
vecMult a b = vecMult (toVec3 0 a) (toVec3 0 b)

toVec3 :: a -> Vector a -> Vector a
toVec3 z (Vector2D x y) = Vector3D x y z
toVec3 _ v@Vector3D {}  = v

data Nat = Z | S Nat deriving Show

instance Eq Nat where
  (==) Z Z         = True
  (==) (S _) Z     = False
  (==) Z (S _)     = False
  (==) (S a) (S b) = a == b

instance Ord Nat where
  compare Z Z         = EQ
  compare Z (S _)     = LT
  compare (S _) Z     = GT
  compare (S a) (S b) = compare a b

instance Num Nat where
  (+) a Z     = a
  (+) a (S b) = S a + b
  (*) _ Z     = Z
  (*) a (S Z) = a
  (*) a (S b) = (a * b) + a
  abs a = a
  signum Z = Z
  signum _ = S Z
  fromInteger 0 = Z
  fromInteger i = S (fromInteger (i - 1))
  (-) a Z         = a
  (-) (S a) (S b) = a - b
  (-) Z _         = error "(-) isn't total on Nat"

instance Enum Nat where
  toEnum = fromInteger . toInteger
  fromEnum Z     = 0
  fromEnum (S n) = 1 + fromEnum n
  succ = S
  pred (S n) = n
  pred Z     = error "There is no `pred' for Z :: Nat"

instance Real Nat where
  toRational n = toInteger n % 0

instance Integral Nat where
  toInteger Z     = 0
  toInteger (S a) = 1 + toInteger a
  div a b = if a <= b
            then 0
            else 1 + ((a - b) `div` b)
  mod a b = if a <= b
            then a
            else mod (a - b) b
  divMod a b = if a <= b
               then (0, a)
               else (1 + dr, mr)
                 where
                   (dr, mr) = divMod (a - b) b
  quotRem = divMod
  quot = div
  rem = mod

evenNat :: Nat -> Bool
evenNat Z     = True
evenNat (S n) = oddNat n

oddNat :: Nat -> Bool
oddNat Z     = False
oddNat (S n) = evenNat n
