module ADT.Easy
       ( DayOfWeek
       , nextDay
       , afterDays
       , isWeekend
       , daysToParty
       , Fighter
       , HolyKnight
       , EvilBeast
       , UnknownSlime
       , attack
       , FightResult
       , fight
       , Vector(Vector2D, Vector3D)
       , length
       , vecSum
       , scalarMult
       , distVec
       , vecMult
       , Nat(S, Z)
       ) where

import           ADT.Hard
import           Prelude  hiding (length)
