module ADT.HardSpec where

import Data.List(intercalate)

import ADT.Hard
import Test.Hspec

knight = HolyKnight "Arthas" 7 100
troll = EvilBeast "Troll leader from first chapter" 12 47
slime = UnknownSlime "green" 37

spec :: Spec
spec = describe "ADT.Hard" $ do
         describe "days of week" $ do
           describe "next day" $ do
             it "returns next day" $ do
               nextDay Monday `shouldBe` Tuesday
               nextDay Sunday `shouldBe` Monday
           describe "after days" $ do
             it "say what day will be after x days" $ do
               afterDays 1 Monday `shouldBe` Tuesday
               afterDays 7 Monday `shouldBe` Monday
           describe "is weekend" $ do
             it "is?" $ do
               isWeekend Monday `shouldBe` False
               isWeekend Sunday `shouldBe` True
           describe "days to party" $ do
             it "say how long you should do something useful" $ do
               daysToParty Monday `shouldBe` 4
               daysToParty Friday `shouldBe` 0
         describe "monsters & knights" $ do
           describe (intercalate "\n\t" ["there is HolyKnight & monsters:", show knight, show troll, show slime]) $ do
             it "they can fight each other" $ do
               frLeftWon (fight (knight, knight)) `shouldBe` True
               frLeftWon (fight (knight, troll)) `shouldBe` True
               frLeftWon (fight (troll, slime)) `shouldBe` True
