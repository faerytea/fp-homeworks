module ListPatternMatching.HardSpec where

import Test.Hspec
import ListPatternMatching.Hard

advancedTests    = [ "+1", "1 +1", "-1 +1", "+1 -1"]
advancedMustFail = ["1+1", "++1", "-+1", "+-1", "1 + 1"]

spec :: Spec
spec = describe "ListPatternMatching.Hard" $ do
         describe "remove at" $ do
           it "removes element on specified position and returns (Mayme e, [e])" $ do
             removeAt 0 [1,2,3] `shouldBe` (Just 1, [2,3])
             removeAt 7 [1,2,3] `shouldBe` (Nothing, [1,2,3])
         describe "collect every" $ do
           it "collects every x element in list" $ do
             collectEvery 3 [1..8] `shouldBe` ([1,2,4,5,7,8], [3,6])
             collectEvery 10 [1..8] `shouldBe` ([1..8], [])
         describe "string sum" $ do
           it "sums ints in string" $ do
             map stringSum advancedTests `shouldBe` [1,2,0,0]
         describe "merge sort" $ do
           it "is sort" $ do
             mergeSort [2, 1, 0, 3, 10, 5] `shouldBe` [0, 1, 2, 3, 5, 10]