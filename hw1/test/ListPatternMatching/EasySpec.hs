module ListPatternMatching.EasySpec where

import Test.Hspec
import ListPatternMatching.Easy

passTests :: [String]
passTests = [ "1", "1 2 3", " 1", "1 ", "\t1\t", "\t12345\t", "010 020 030"
            , " 123 456 789 ", "-1", "-1 -2 -3", "\t-12345\t", " -123 -456 -789 "
            , "\n1\t\n3   555  -1\n\n\n-5", "123\t\n\t\n\t\n321 -4 -40"
            ]

mustFail  = ["asd", "1-1", "1.2", "--2", "+1", "1+"]

spec :: Spec
spec = describe "ListPatternMatching.Easy" $ do
         describe "string sum" $ do
           it "consumes string of ints and returns sum" $ do
             stringSum "1 2 3" `shouldBe` 6
           it "also passes tests from hackmd.io" $ do
             map stringSum passTests `shouldBe` [1,6,1,1,1,12345,60
                                                ,123+456+789,-1,-1-2-3,-12345,-123-456-789
                                                ,4+555-1-5,123+321-4-40]
         describe "remove at" $ do
           it "removes element on specified position" $ do
             removeAt 1 [1,2,3] `shouldBe` [1,3]
             removeAt 10 [1,2,3] `shouldBe` [1,2,3]
             removeAt 3 [1..5] `shouldBe` [1,2,3,5]
             removeAt 2 "abc" `shouldBe` "ab"