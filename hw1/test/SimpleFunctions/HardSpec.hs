module SimpleFunctions.HardSpec where

import SimpleFunctions.Hard
import Test.Hspec

spec :: Spec
spec = describe "SimpleFunctions.Hard" $ do
         describe "highest bit" $ do
           it "should return (2^x, x) such that x is the lowest x for any a such that 2^x <= a" $ do
             highestBit 129 `shouldBe` (128, 7)
         describe "order3" $ do
           it "will order (a,b,c)" $ do
             order3 (7,1,2) `shouldBe` (1,2,7)
         describe "smart replicate" $ do
           it "replicates Int exactly Int times for all Ints in list" $ do
             smartReplicate [1,2,3] `shouldBe` [1,2,2,3,3,3]
         describe "contains" $ do
           it "filters list which have x" $ do
             contains 7 [[], [1,2,3],[7],[14,7,2],[],[1..20]] `shouldBe` [[7],[14,7,2],[1..20]]