module SimpleFunctions.EasySpec where

import SimpleFunctions.Easy

import Test.Hspec

spec :: Spec
spec = describe "SimpleFunctions.Easy" $ do
         describe "highest bit" $ do
           it "should return 2^x such that x is the lowest x for any a such that 2^x <= a" $ do
             highestBit 129 `shouldBe` 128