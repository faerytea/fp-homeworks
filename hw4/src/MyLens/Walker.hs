{-# LANGUAGE FlexibleContexts #-}

module MyLens.Walker
       ( up
       , down
       , dialog
       , mainLoop
       ) where

import           Control.Lens        (has, (^..), (^?))

import           Control.Monad.State (MonadIO (..), MonadState (..), runStateT)

import           Data.Bifunctor      (bimap)
import           Data.List           (partition)

import           System.Exit         (exitSuccess)

import           MyLens.Filesystem   (FS (..), cd, ls, lsFS, _Dir)

printStat :: MonadIO m => ([FS], Int, Int) -> m ()
printStat (fss, fs, ds) = liftIO $ do
    putStrLn $ "Current dir: " ++ foldl (\s f -> '/' : _name f ++ s) "/" fss
    putStrLn $ "Files: " ++ show fs
    putStrLn $ "Dirs:  " ++ show ds

countStat :: FS -> (Int, Int)
countStat fs = bimap length length $ partition (has _Dir) (fs^..lsFS)

initFS :: ( MonadIO                     m
          , MonadState ([FS], Int, Int) m
          ) => m ()
initFS = do
    (fs:_, _, _) <- get
    let (mds,mfs) = countStat fs
    put ([fs], mfs, mds)

up :: ( MonadIO                     m
      , MonadState ([FS], Int, Int) m
      ) => m ()
up = do
    (fs:stack, files, dirs) <- get
    if null stack
    then liftIO $ putStrLn "Error: root directory."
    else let (mds,mfs) = countStat fs
             newState  = (stack, files - mfs, dirs - mds)
         in do
             put newState
             printStat newState

down :: ( MonadIO                     m
        , MonadState ([FS], Int, Int) m
        ) => FilePath -> m ()
down s = do
    (fs:stack, files, dirs) <- get
    case fs ^? cd s of
        Just good ->
            let (mds,mfs) = countStat good
                newState  = (good:fs:stack, files + mfs, dirs + mds)
            in do
                put newState
                printStat newState
        Nothing   -> liftIO $ putStrLn $ "No directory `" ++ s ++ "'"

list :: ( MonadIO                     m
        , MonadState ([FS], Int, Int) m
        ) => m ()
list = do
    (fs:_, _, _) <- get
    liftIO $ putStrLn $ unlines $ fs ^.. ls

dialog :: ( MonadIO                     m
          , MonadState ([FS], Int, Int) m
          ) => m ()
dialog = do
    liftIO $ putStr "> "
    line <- liftIO getLine
    let ws = words line
    case ws of
        []       -> return ()
        ["q"]    -> liftIO exitSuccess
        ["up"]   -> up
        ["cd",s] -> down s
        ["ls"]   -> list
        _        -> liftIO $ putStrLn "Wrong command!"

mainLoop :: ( MonadIO                     m
            ) => FS -> m ()
mainLoop fs = runStateT (sequence_ $ initFS : repeat dialog) ([fs], 0, 0) >> pure ()
