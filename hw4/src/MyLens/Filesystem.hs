{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# LANGUAGE TemplateHaskell  #-}

module MyLens.Filesystem
       ( FS(..)
       , fakeFS
       , cd
       , ls
       , lsFS
       , file
       , changeExt
       , allNames
       , rm
       , _Dir
       , _File
       , scanDirectory
       ) where

import           Control.Lens     (Traversal, filtered, has, makeLenses, traversed, (%~),
                                   (^.))
import           Control.Lens.TH

import           Control.Monad    (mapM)

import           System.Directory (doesDirectoryExist, listDirectory, makeAbsolute)

data FS
    = Dir  { _name     :: FilePath  -- название папки, не полный путь
           , _contents :: [FS]
           }
    | File { _name :: FilePath  -- название файла, не полный путь
           }
    deriving Show

fakeFS :: FS
fakeFS = Dir "root" [ Dir "code" [ File "abc.cpp"
                                 , File "test.cpp" ]
                    , Dir "pictures" [ File "doge.png"
                                     , Dir "memes" [ File "troll.png"
                                                   , File "jackal.jpeg.jpg" ]
                                     , File "screenfetch.png" ]
                    , Dir "www" [ File "favicon.ico"
                                , File "index.html"
                                , File "logo.png" ]
                    , File "init.sh" ]

scanDirectory :: FilePath -> IO FS
scanDirectory path = do
    absPath <- makeAbsolute path
    content <- listDirectory absPath
    list <- mapM (convert absPath) content
    return $ Dir (reverse $ takeWhile (/= '/') $ reverse absPath) list
  where
    convert :: FilePath -> FilePath -> IO FS
    convert p s = do
        let ap = p ++ '/':s
        isDir <- doesDirectoryExist ap
        if isDir then scanDirectory ap
                 else return $ File s

makeLenses ''FS
makePrisms ''FS

cd :: FilePath -> Traversal FS FS FS FS
cd to = contents . traversed . filtered (\dir -> has _Dir dir && (dir ^. name) == to)

ls :: Traversal FS FS FilePath FilePath
ls = contents . traversed . name

lsFS :: Traversal FS FS FS FS
lsFS = contents . traversed

file :: FilePath -> Traversal FS FS FilePath FilePath
file s = contents . traversed . filtered (\f -> has _File f && f^.name == s) . name

changeExt :: FilePath -> FilePath -> FS -> FS
changeExt from to = ls %~ renaming
  where
    rf = reverse from
    fl = length from
    renaming s
        | take fl (reverse s) == rf = take (length s - fl) s ++ to
        | otherwise                 = s

allNames :: FS -> [FilePath]
allNames fs = (fs ^. contents) >>= \f -> f^.name : allNames f

rm :: FS -> FilePath -> FS
rm from dir = (contents %~ filter good) from  --from %~ contents filter good
  where
    good f = has _File f
          || not (null $ f ^. contents)
          || f ^. name /= dir
