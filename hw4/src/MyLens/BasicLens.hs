{-# LANGUAGE RankNTypes #-}

module MyLens.BasicLens
       ( Lens
       , Lens'
       , set
       , view
       , over
       , (.~)
       , (^.)
       , (%~)
       , _1
       , _2
       , lens
       , choosing
       , (<%~)
       , (<<%~)
       ) where

import           Control.Applicative    (Const (Const, getConst))
import           Control.Monad.Identity (Identity (Identity, runIdentity))

type Lens s t a b = forall f . Functor f => (a -> f b) -> s -> f t

type Lens' s a = Lens s s a a

set  :: Lens s t a b -> b -> s -> t         -- set    value (setter)
set l f o = runIdentity $ l (const $ Identity f) o

view :: Lens s t a b -> s -> a              -- lookup value (getter)
view l o = getConst $ l Const o

over :: Lens s t a b -> (a -> b) -> s -> t  -- change value (modifier)
over l m o = runIdentity $ l (fmap m . Identity) o

(.~) :: Lens s t a b -> b -> s -> t
(.~) = set

(^.) :: s -> Lens s t a b -> a
(^.) o l = view l o

(%~) :: Lens s t a b -> (a -> b) -> s -> t
(%~) = over

_1 :: Lens (a, x) (b, x) a b
_1 m (f, s) = (\x -> (x, s)) <$> m f

_2 :: Lens (x, a) (x, b) a b
_2 m (f, s) = (\x -> (f, x)) <$> m s

lens :: (s -> a) -> (s -> b -> t) -> Lens s t a b
lens getter setter = \m o -> fmap (setter o) (m $ getter o)

choosing :: forall s1 s2 t1 t2 a b.
            Lens s1 t1 a b
         -> Lens s2 t2 a b
         -> Lens (Either s1 s2) (Either t1 t2) a b
choosing l1 l2 = lens getter setter
  where
    getter (Right o) = view l2 o
    getter (Left o)  = view l1 o
    setter (Right o) v = Right $ set l2 v o
    setter (Left o)  v = Left  $ set l1 v o

(<%~) :: Lens s t a b -> (a -> b) -> s -> (b, t)
(<%~) l f s = (f $ s ^. l, (l %~ f) s)

(<<%~) :: Lens s t a b -> (a -> b) -> s -> (a, t)
(<<%~) l f s = (s ^. l, (l %~ f) s)
