{-# LANGUAGE TemplateHaskell #-}

module Templates
       ( chooseByIndices
       , Scribe(..)
       , generateInstance
       ) where

import           Control.Monad       (foldM)

import qualified Data.IntMap.Strict  as IM (empty, insert, lookup, member, (!))
import qualified Data.Text           as T (Text)

import           Language.Haskell.TH (Dec, Exp, Name, Q, conT, lamE, newName, runQ, tupE,
                                      tupP, varE, varP, wildP)

chooseByIndices :: Int -> [Int] -> Q Exp
chooseByIndices n is = do
    let constructor im x
            | x `IM.member` im = return im
            | otherwise        = newName ('x' : show x) >>= \name -> return $ IM.insert x name im
    names <- foldM constructor IM.empty is
    let mapper im x = case IM.lookup x im of
                           Just name -> varP name
                           Nothing   -> wildP
    lamE [tupP $ map (mapper names) [0..(n-1)]] (tupE $ map (varE . (IM.!) names) is)

class Show a => Scribe a where
    scribe :: a -> T.Text

generateInstance :: Name -> Q [Dec]
generateInstance name =
    runQ [d|instance Scribe $(conT name) where
                scribe = (pack . show)|]
