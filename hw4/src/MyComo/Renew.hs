{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module MyComo.Renew where

import           Control.Comonad (Comonad (..))

data Renew s e a = Renew (e -> a) s

class Monoid e => MonoidAction s e where
    act :: s -> e -> s

instance Integral n => MonoidAction n [a] where
    act c l = fromIntegral (toInteger c + toInteger (length l))

instance Functor (Renew s e) where
    fmap f (Renew u s) = Renew (f . u) s

instance MonoidAction s e => Comonad (Renew s e) where
    extract (Renew u _) = u mempty
    duplicate (Renew u s) = Renew
        (\e -> Renew (\e2 -> u (e `mappend` e2)) s) s
    extend f (Renew u s) = Renew
        (\e -> f (Renew (\e2 -> u (e `mappend` e2)) s)) s

