module MyComo.RoseZipper where

import Control.Comonad

data Tree a = Node a [Tree a] deriving (Show, Eq)

instance Functor Tree where
    fmap f (Node x ts) = Node (f x) (map (fmap f) ts)

instance Comonad Tree where
    extract (Node x _) = x
    duplicate t@(Node _ ts) = Node t (map duplicate ts)