{-# LANGUAGE RecordWildCards #-}

module MyComo.Builder where

import           Control.Comonad(extract)

import           Data.Text(Text)

data ProjectSettings = ProjectSettings
     { settingsBenchs :: Bool  -- ^ enable benchmarks for project?
     , settingsGithub :: Bool  -- ^ set up github     for project?
     , settingsTravis :: Bool  -- ^ set up Travis CI  for project?
     }

data Project = Project
     { projectName :: Text
     , hasBenchs   :: Bool
     , hasGithub   :: Bool
     , hasTravis   :: Bool
     } deriving (Show)

instance Monoid ProjectSettings where
    mempty = ProjectSettings False False False
    mappend (ProjectSettings ab ag at) (ProjectSettings bb bg bt) =
        ProjectSettings (ab || bb) (ag || bg) (at || bt)

type ProjectBuilder = ProjectSettings -> Project

benchs :: ProjectBuilder -> Project
benchs pb = pb $ ProjectSettings True False False

github :: (ProjectSettings -> Project) -> Project
github pb = pb $ ProjectSettings False True False

travis :: (ProjectSettings -> Project) -> Project
travis pb = pb $ if hasGithub (extract pb) then ProjectSettings False True True
                                           else mempty

buildProject :: Text -> ProjectBuilder
buildProject name =
    \ProjectSettings {..} -> Project name settingsBenchs settingsGithub settingsTravis

