{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module TemplatesSpec where

import           Test.Hspec

import           Data.Text  (Text, pack)
import           Templates

data Test = T1 | T2 | T3 deriving Show

generateInstance ''Test
generateInstance ''Bool

spec :: Spec
spec = describe "Templates" $ do
         describe "Choose by indices" $ do
           it "$(chooseByIndices 4 [2, 0])          (\"hello\", 10, [4,3], 2)" $
             $(chooseByIndices 4 [2, 0]) ("hello" :: String, 10 :: Int, [4,3] :: [Int], 2 :: Integer)
               `shouldBe` ([4, 3] :: [Int], "hello" :: String)
           it "$(chooseByIndices 4 [1, 1, 3, 1, 1]) (\"hello\", 10, [4,3], 2)" $
             $(chooseByIndices 4 [1, 1, 3, 1, 1]) ("hello" :: String, 10 :: Integer, [4,3] :: [Integer], 2 :: Int)
               `shouldBe` (10 :: Integer, 10 :: Integer, 2 :: Int, 10 :: Integer, 10 :: Integer)
         describe "Scribe instances" $ do
           it "Test: T1, T2, T3" $ do
             scribe T2 `shouldBe` ("T2" :: Text)
             scribe T2 `shouldBe` ("T2" :: Text)
             scribe T2 `shouldBe` ("T2" :: Text)
           it "Bool: True, False" $ do
             scribe True `shouldBe` ("True" :: Text)
             scribe False `shouldBe` ("False" :: Text)
