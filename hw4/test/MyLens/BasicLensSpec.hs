module MyLens.BasicLensSpec where

import           Test.Hspec

import           MyLens.BasicLens

-- TODO: Laws

spec :: Spec
spec = let pair = (1 :: Int, "hell")
       in describe "MyLens.BasicLens" $
         describe "Basic: (1, \"hell\")" $ do
           it "set" $ do
             set _1 (421 :: Integer) pair `shouldBe` (421 :: Integer, snd pair)
             set _2 "a" pair `shouldBe` (fst pair, "a")
           it "view" $ do
             view _1 pair `shouldBe` fst pair
             view _2 pair `shouldBe` snd pair
           it "over" $ do
             over _1 (+1) pair `shouldBe` (fst pair + 1, snd pair)
             over _2 show pair `shouldBe` (fst pair, show $ snd pair)