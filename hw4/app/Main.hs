module Main where

import           Control.Monad      (guard)

import           System.Environment (getArgs)

import           MyLens.Filesystem  (fakeFS, scanDirectory)
import           MyLens.Walker      (mainLoop)

main :: IO ()
main = do
    args <- getArgs
    guard (length args < 2)
    fs <- if length args == 1 then scanDirectory (head args)
                              else return fakeFS
    _ <- mainLoop fs
    pure ()
