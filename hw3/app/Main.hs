{-# LANGUAGE FlexibleContexts #-}

module Main
       ( main
       ) where

import           Prelude              hiding (getLine, readFile)

import           Data.Functor         (void)
import           Data.Text.IO         (readFile)

import           Control.Monad.Except (MonadError, catchError, runExceptT)
import           Control.Monad.Trans  (MonadIO, liftIO)

import           System.Environment   (getArgs)

import           SimpleLanguage       (EvaluationError, createProgram)

main :: IO ()
main = do
    args <- getArgs
    if null args
    then putStrLn "Usage: slang <file>"
    else do
        content <- readFile (head args)
        _ <- runExceptT (catchError (void $ createProgram content) handler)
        return ()

handler :: (MonadIO m, MonadError EvaluationError m) => EvaluationError -> m ()
handler err = liftIO $ do
    liftIO $ putStrLn "Error:"
    liftIO $ print err
