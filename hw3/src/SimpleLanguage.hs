{-# LANGUAGE FlexibleContexts #-}

module SimpleLanguage
       ( createProgram
       , EvaluationError
       ) where

import           Data.Text                      (Text)

import           Control.Monad.Except           (MonadError, throwError)
import           Control.Monad.IO.Class         (MonadIO)
import           Control.Monad.State            (execStateT)

import           Text.Megaparsec                (parse)


import           SimpleLanguage.Internal        (Bindings, EvaluationError (..),
                                                 initialBindings)
import           SimpleLanguage.StatementParser (statementList)
import           SimpleLanguage.Statements      (compile)

createProgram :: ( MonadError EvaluationError m
                 , MonadIO m)
              => Text -> m Bindings
createProgram p = case parse statementList "" p of
    Left err -> throwError $ NoParse (show err)
    Right sl -> execStateT (compile sl) initialBindings
