module SimpleLanguage.ExpressionParser
       ( expression
       , ident
       , parseExpression
       ) where

import           Data.List                  (foldl')
import           Data.List.NonEmpty         (NonEmpty (..))
import qualified Data.Text                  as T

import           Text.Megaparsec            (Dec, ParseError, alphaNumChar, char,
                                             digitChar, letterChar, many, parse, skipMany,
                                             skipSome, some, spaceChar, string, try,
                                             (<|>))
import           Text.Megaparsec.Text       (Parser)

import           SimpleLanguage.Expressions (Expr (..))

parseExpression :: T.Text -> Either (ParseError Char Dec) Expr
parseExpression = parse expression ""

expression :: Parser Expr
expression = skipMany spaceChar
         *> (try addsub
         <|> parens)

parens :: Parser Expr
parens = do
    _ <- char '('
    skipMany spaceChar
    expr <- expression
    skipMany spaceChar
    _ <- char ')'
    return expr

addsub :: Parser Expr
addsub = binOpParser divmul $ ('+', (:+)) :|
                            [ ('-', (:-)) ]

divmul :: Parser Expr
divmul = binOpParser expop $ ('/', (:/)) :|
                           [ ('%', (:%))
                           , ('*', (:*)) ]

expop :: Parser Expr
expop = binOpParser atom $ ('^', (:^)) :| []

atom :: Parser Expr
atom = try ident <|> try number <|> try letexp <|> parens

ident :: Parser Expr
ident = do
    f <- letterChar
    r <- many alphaNumChar
    return $ Var $ T.pack $ foldr (:) [] (f:r)

number :: Parser Expr
number = do
    n <- some digitChar
    return $ Const $ read $ foldr (:) [] n

letexp :: Parser Expr
letexp = do
    _ <- char '('
    skipMany spaceChar
    _ <- string "let"
    skipSome spaceChar
    var <- ident
    skipMany spaceChar
    _ <- string "="
    skipMany spaceChar
    binding <- expression
    skipMany spaceChar
    _ <- string "in"
    skipMany spaceChar
    expr <- expression
    skipMany spaceChar
    _ <- char ')'
    let (Var name) = var in return $ Local name binding expr

secondParser :: Char -> Parser Expr -> (Expr -> Expr -> Expr) -> Parser (Expr -> Expr)
secondParser sym rp op = try $ do
    skipMany spaceChar
    _ <- char sym
    skipMany spaceChar
    r <- rp
    return (`op` r)

binOpParser :: Parser Expr -> NonEmpty (Char, Expr -> Expr -> Expr) -> Parser Expr
binOpParser deeper ((sym, o):|ops) = do
  l <- deeper
  sec <- many
       $ foldr ((<|>) . (\(c, op) -> secondParser c deeper op)) (secondParser sym deeper o) ops
  return (foldl' (\e f -> f e) l sec)

