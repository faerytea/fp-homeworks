{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeOperators    #-}
{-# LANGUAGE OverloadedStrings #-}

module SimpleLanguage.Expressions
       ( Expr(..)
       , EvaluationError(..)
       , compute
       , eval
       , Bindings
       ) where

import           Control.Monad           (liftM2)
import           Control.Monad.Except    (MonadError, throwError)
import           Control.Monad.Reader    (MonadReader, ask, local, runReaderT)

import qualified Data.Map.Strict         as DM
import qualified Data.Text               as T

import           SimpleLanguage.Internal (Bindings,EvaluationError(..),Expr(..))

getValue :: ( MonadError  EvaluationError m
            , MonadReader Bindings  m)
         => T.Text -> m Integer
getValue s = ask >>= \m ->
    maybe (throwError $ ExpressionError ("no such variable " ++ T.unpack s) (Var s)) return (DM.lookup s m)

eval :: Expr -> Either EvaluationError Integer
eval expr = runReaderT (compute expr) DM.empty

compute :: ( MonadError  EvaluationError m
           , MonadReader Bindings  m)
        => Expr -> m Integer
compute (Const v) = return v
compute (a :+ b) = magicLift (+) a b
compute (a :- b) = magicLift (-) a b
compute (a :* b) = magicLift (*) a b
compute (a :/ b) = checkedMagic (\_ y -> y /= 0) div a b (ExpressionError "division by zero!" (a:/b))
compute (a :% b) = checkedMagic (\_ y -> y /= 0) mod a b (ExpressionError "division by zero!" (a:%b))
compute (a :^ b) = checkedMagic (\_ y -> y >= 0) (^) a b (ExpressionError "negative exponent" (a:^b))
compute (Local var val expr) = do
    res <- compute val
    local (DM.insert var res) (compute expr)
compute (Var name) = getValue name

checkedMagic :: ( MonadError  EvaluationError m
                , MonadReader Bindings  m)
             => (Integer -> Integer -> Bool)          -- check that shows that computation can be performed
                  -> (Integer -> Integer -> Integer)  -- actual computation
                  -> Expr                             -- a
                  -> Expr                             -- b
                  -> EvaluationError                  -- error message
                  -> m Integer
checkedMagic check op a b err =
    compute a >>= (\x -> (compute b >>= \ y -> checkedOp check op x y err))

checkedOp :: (MonadError e m) => (a -> b -> Bool) -> (a -> b -> c) -> a -> b -> e -> m c
checkedOp check op a b err = if check a b then return $ op a b else throwError err

magicLift :: ( MonadError  EvaluationError m
             , MonadReader Bindings  m)
          => (Integer -> Integer -> Integer) -> Expr -> Expr -> m Integer
magicLift op x y = liftM2 op (compute x) (compute y)
