{-# LANGUAGE FlexibleContexts #-}

module SimpleLanguage.Statements
       ( Statement(..)
       , EvaluationError(..)
       , compile
       , evalWithState
       ) where

import qualified Data.Map.Strict            as DM
import qualified Data.Text                  as T

import           Control.Monad              (join)
import           Control.Monad.Cont         (ContT (..), callCC, runContT)
import           Control.Monad.Except       (MonadError, lift)
import           Control.Monad.Reader       (runReaderT)
import           Control.Monad.State        (MonadState, get, modify)
import           Control.Monad.Trans        (MonadIO, liftIO)

import           SimpleLanguage.Expressions (Expr (..), compute)
import           SimpleLanguage.Internal    (Bindings, EvaluationError (..),
                                             Statement (..))
import           SimpleLanguage.Variables   (addNew, mutate, scoped)

compile :: ( MonadError EvaluationError m
           , MonadState Bindings       m
           , MonadIO m)
        => [Statement] -> m ()
compile = compileL return

compileL :: ( MonadError EvaluationError m
            , MonadState Bindings       m
            , MonadIO m)
         => (() -> ContT () m ()) -> [Statement] -> m ()
compileL abort p = runContT (mapM_ (`statementToAction` abort) p) return

statementToAction :: ( MonadError EvaluationError m
                     , MonadState Bindings       m
                     , MonadIO m )
                  => Statement -> (() -> ContT () m ()) -> ContT () m ()
statementToAction (VariableDefinition name expr) _ = lift $ computeVar name expr addNew
statementToAction (VariableMutation   name expr) _ = lift $ computeVar name expr mutate
statementToAction (Print expr) _ = lift $ get >>= evalWithState expr >>= liftIO . print
statementToAction (Scan var) _ = liftIO getLine >>= modify . DM.insert var . read
statementToAction (LoopWithCount name fromE toE sl) _ = do
    s <- lift get
    from <- lift $ evalWithState fromE s
    to   <- lift $ evalWithState toE   s
    callCC $ \f -> lift
         $ scoped name from
         $ compileL f
         $ join
         $ (if to < from then [] else sl)
             : map (\x -> VariableMutation name (Const x) : sl) [from+1..to]
statementToAction Break abort = abort ()

computeVar :: ( MonadError EvaluationError m
              , MonadState Bindings       m )
           => T.Text -> Expr -> (T.Text -> Integer -> m ()) -> m ()
computeVar name expr method = get >>= evalWithState expr >>= method name

evalWithState :: (MonadError EvaluationError m) => Expr -> Bindings -> m Integer
evalWithState expr = runReaderT (compute expr)
