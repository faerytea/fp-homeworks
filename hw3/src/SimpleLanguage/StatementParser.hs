module SimpleLanguage.StatementParser
       ( statement
       , parseStatement
       , statementList
       ) where

import           Data.Char                       (isSpace)
import           Data.Text                       (Text)

import           Text.Megaparsec
import           Text.Megaparsec.Text            (Parser)

import           SimpleLanguage.ExpressionParser (expression, ident)
import           SimpleLanguage.Expressions      (Expr (Var))
import           SimpleLanguage.Statements       (Statement (..))

parseStatement :: Text -> Either (ParseError Char Dec) Statement
parseStatement = parse statement ""

statement :: Parser Statement
statement = try mutDef <|> scan <|> out <|> forLoop <|> breakP <|> mutation

mutDef :: Parser Statement
mutDef = do
    _ <- string "mut"
    skipSome spaceChar
    (Var name, expr) <- varEqExp
    return $ VariableDefinition name expr

mutation :: Parser Statement
mutation = do
    (Var name, expr) <- varEqExp
    return $ VariableMutation name expr

varEqExp :: Parser (Expr, Expr)
varEqExp = do
    var <- ident
    skipMany spaceChar
    _ <- char '='
    skipMany spaceChar
    expr <- expression
    return (var, expr)

scan :: Parser Statement
scan = do
    _ <- char '>'
    skipMany spaceChar
    (Var name) <- ident
    return $ Scan name

out :: Parser Statement
out = do
    _ <- char '<'
    skipMany spaceChar
    expr <- expression
    return $ Print expr

forLoop :: Parser Statement
forLoop = do
    _ <- string "::"
    skipMany spaceChar
    (Var x) <- ident
    skipMany spaceChar
    from <- expression
    skipMany spaceChar
    _ <- string "->"
    skipMany spaceChar
    to <- expression
    skipMany spaceChar
    actions <- block
    return $ LoopWithCount x from to actions

block :: Parser [Statement]
block = do
    _ <- char '"'
    res <- statementList
    skipMany spaceChar
    _ <- char '"'
    return res

statementList :: Parser [Statement]
statementList = many $ do
    skipMany spaceChar
    res <- statement
    skipMany $ satisfy (\c -> c /= '\n' && c /= '\r' && isSpace c)
    _ <- oneOf ['\n', '\r']
    return res

breakP :: Parser Statement
breakP = string "!!" >> return Break
