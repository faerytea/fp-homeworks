{-# LANGUAGE FlexibleContexts #-}

module SimpleLanguage.Variables
       ( addNew
       , mutate
       , scoped
       ) where

import qualified Data.Map.Strict         as DM
import qualified Data.Text               as T

import           Control.Monad.Except    (MonadError, throwError)
import           Control.Monad.State     (MonadState, get, put)

import           SimpleLanguage.Internal (Bindings, EvaluationError (..), Expr (..),
                                          Statement (..))

addNew :: ( MonadState Bindings m
          , MonadError EvaluationError m
          )
       => T.Text -> Integer -> m ()
addNew name value = do
    old <- get
    if name `DM.member` old
    then throwError $ StatementError
                      "duplicate defenition of mutable variable"
                      (VariableDefinition name $ Const value)
    else put (DM.insert name value old)

mutate :: ( MonadState Bindings m
          , MonadError EvaluationError m
          )
       => T.Text -> Integer -> m ()
mutate name value = do
    old <- get
    if name `DM.member` old
    then put (DM.insert name value old)
    else throwError $ StatementError
                      "duplicate defenition of mutable variable"
                      (VariableMutation name $ Const value)

scoped :: ( MonadState Bindings m
          , MonadError EvaluationError m
          )
       => T.Text -> Integer -> m a -> m ()
scoped name value action = do
    old <- get
    addNew name value
    _ <- action
    new <- get
    put $ new `DM.intersection` old
