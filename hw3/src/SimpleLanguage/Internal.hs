module SimpleLanguage.Internal
       ( Bindings
       , initialBindings
       , Expr(..)
       , Statement(..)
       , EvaluationError(..)
       ) where

import qualified Data.Map.Strict as DM (Map, empty)
import           Data.Text       (Text, unpack)

type Bindings = DM.Map Text Integer

initialBindings :: Bindings
initialBindings = DM.empty

data Expr = Const Integer
          | Var Text
          | Expr :+ Expr
          | Expr :- Expr
          | Expr :* Expr
          | Expr :/ Expr
          | Expr :% Expr
          | Expr :^ Expr
          | Local Text Expr Expr

instance Show Expr where
    show (Const i) = show i
    show (Var s) = unpack s
    show (a :+ b) = '(' : show a ++ " + " ++ show b ++ ")"
    show (a :- b) = '(' : show a ++ " - " ++ show b ++ ")"
    show (a :* b) = '(' : show a ++ " * " ++ show b ++ ")"
    show (a :/ b) = '(' : show a ++ " / " ++ show b ++ ")"
    show (a :% b) = '(' : show a ++ " % " ++ show b ++ ")"
    show (a :^ b) = '(' : show a ++ " ^ " ++ show b ++ ")"
    show (Local s binding expr) = "(let " ++ unpack s ++ " = " ++ show binding ++ " in " ++ show expr ++ ")"

data Statement = VariableDefinition Text Expr
               | VariableMutation Text Expr
               | Print Expr
               | Scan Text
               | LoopWithCount Text Expr Expr [Statement]
               | Break

instance Show Statement where
    show (VariableDefinition name expr) = "mut " ++ unpack name ++ " = " ++ show expr
    show (VariableMutation name expr)   = unpack name ++ " = " ++ show expr
    show (Print expr)                   = "< " ++ show expr
    show (Scan name)                    = "> " ++ unpack name
    show (LoopWithCount v f t _)        = ":: " ++ unpack v ++ " " ++ show f ++ " -> " ++ show t ++ "\"...\""
    show Break                          = "!!"

data EvaluationError = StatementError String Statement
                     | ExpressionError String Expr
                     | NoParse String

instance Show EvaluationError where
    show (StatementError message st) = message ++ "\n :> " ++ show st
    show (ExpressionError message e) = message ++ "\n => " ++ show e
    show (NoParse message) = message
